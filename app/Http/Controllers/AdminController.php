<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view("admin.auth.login");
    }

    public function master_pelajaran(Request $request){
        return view("admin.pelajaran.master_pelajaran");
    }
    public function tambah_pelajaran(Request $request){
        return view("admin.pelajaran.tambah_master_pelajaran");
    }

    public function master_jenjang(Request $request){
        return view("admin.jenjang.master_jenjang");
    }
    public function tambah_jenjang(Request $request){
        return view("admin.jenjang.tambah_master_jenjang");
    }
    public function member(Request $request){
        return view("admin.member.index");
    }

    public function detail(Request $request){
        return view("admin.member.detail");
    }

    public function detail_murid(Request $request){
        return view("admin.member.detail_murid");
    }
}
