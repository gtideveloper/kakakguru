<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use DB;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function showLoginForm(Request $request){
        return view("auth.login",compact('request'));
    }

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $check = DB::table("users")
            ->join("users_detail","users.id","=","users_detail.user_id")
            ->where(["email"=>$request->email])
            ->count();
            
        if($check <= 0){
            $this->redirectTo = "/profil";
        }

        $this->middleware('guest')->except('logout');
    }

    protected function sendMessage($sender_address, $sender_name, $email, $subject, $body)
    {
        $transport = (new Swift_SmtpTransport($this->host, $this->port))
            ->setUsername($this->username)
            ->setPassword($this->password);

        $mailer    = new Swift_Mailer($transport);

        $message   = (new Swift_Message($subject))
            ->setFrom($sender_address, $sender_name)
            ->setTo($email)
            ->setBody($body);


        return $mailer->send($message);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }
}
