<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class ProfileController extends Controller
{
//     public function __construct()
//     {
//         $this->middleware('auth');
//     }
    public function index(Request $request)
    {
       
        $db = DB::table("users_detail")->where("user_id",Auth::user()->id)->first();
        $db_pendidikan = DB::table("guru_riwayat_pendidikan")->where("user_id",Auth::user()->id)->first();
        $db_pelatihan = DB::table("guru_riwayat_pelatihan")->where("guru_id",Auth::user()->id)->get();
        $db_pelatihan_num = DB::table("guru_riwayat_pelatihan")->where("guru_id",Auth::user()->id)->count();
        return view('profile.index',compact('db','db_pendidikan','db_pelatihan','db_pelatihan_num'));
    }

    public function detail(Request $request)
    {  
        $db = DB::table("users_detail")
            ->select(
                DB::raw("users_detail.*"),
                "email","name","riwayat_pendidikan","jenjang","nama_bidang_studi"
            )
            ->join("users","users_detail.user_id","=","users.id")
            ->join("guru_riwayat_pendidikan","guru_riwayat_pendidikan.user_id","=","users.id")
            ->join("guru_bidang_studi","guru_bidang_studi.guru_id","=","users.id")
            ->where("guru_bidang_studi.bidang_studi",$request->bidang_studi)
            ->first();
        
        if($db){
            $data = [
                "status"=>"success",
                "status_code"=>200,
                "data"=>$db,
            ];
        }else{
            $data = [
                "status"=>"error",
                "status_code"=>204,
                "data"=>$db,
            ];
        }
       
      
        return response()->json($data);
    }
}
