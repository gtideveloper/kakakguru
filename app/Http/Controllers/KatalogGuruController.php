<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Http;

class KatalogGuruController extends Controller
{
    public function index(Request $request)
    {
        $db = DB::table("users_detail")
            ->select(
                "role_id",
                DB::raw("users_detail.*"),
                "guru_bidang_studi.*"
            )
            ->join("users","users.id","=","users_detail.user_id")
            ->join("guru_bidang_studi","guru_bidang_studi.guru_id","=","users_detail.user_id")
            ->where("role_id","guru");

            if($request->kelas){
                $db  = $db->whereRaw("kelas LIKE '%".$request->kelas."%'");
            }
            if($request->jenjang){
                $db  = $db->whereRaw("jenjang LIKE '%".$request->jenjang."%'");
            }
            if($request->pelajaran){
                $db  = $db->whereRaw("nama_bidang_studi LIKE '%".$request->pelajaran."%'");
            }
        $db = $db->get();
       
        return view('katalog_guru',compact('db'));
    }

    public function cart(Request $request)
    {
        $db = DB::table("users_detail")
            ->select(
                "role_id",
                DB::raw("users_detail.*"),
                "guru_bidang_studi.*"
            )
            ->join("users","users.id","=","users_detail.user_id")
            ->join("guru_bidang_studi","guru_bidang_studi.guru_id","=","users_detail.user_id")
            ->where("role_id","guru")
            ->where("bidang_studi",$request->id)
            ->first();

        $guru_jadwal = DB::table("guru_jadwal_pelajaran")->where("bidang_studi_id",$request->id)->get();
       
        $hari = $this->hari_text();

        return view('cart',compact('db','hari','guru_jadwal'));
    }
    protected function hari_text(){
        $hari = [
           "Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu" 
        ];
        return $hari;
    }

    protected function midTransCore(){
         \Midtrans\Config::$serverKey = 'SB-Mid-server-oMMNcoLzuehI84Zs7Ets_QQX';
	 
            $params = array(
                'transaction_details' => array(
                    'order_id' => rand(),
                    'gross_amount' => 10000,
                ),
                'payment_type' => 'gopay',
                'gopay' => array(
                    'enable_callback' => true,                // optional
                    'callback_url' => 'someapps://callback'   // optional
                )
            );
       
         $response = \Midtrans\CoreApi::charge($params);
    }

    protected function midTransSnap($request){
        
        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = 'Mid-server-7uq1xRIy9B24LbIqLTwTNHBf';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = true;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;  

        $params = array(
            'transaction_details' => array(
                'order_id' => $request->no_order,
                'gross_amount' => $request->harga_guru,
            ),
            'customer_details' => array(
                'first_name' => Auth::user()->name,
                'last_name' => '-',
                'email' => Auth::user()->email,
                'phone' => '-',
            ),
        );
         
        return $snapToken = \Midtrans\Snap::getSnapToken($params);
    }

    public function detail_user(Request $request)
    {
        return view('detail_user',compact('db'));
    }

    public function checkout(Request $request)
    {
        $db = DB::table("kelas_pesan")
        ->select(
            DB::raw("kelas_pesan.*"),
            "detail_guru.photos AS photo_guru",
            "murid.name AS nama_murid",
            "murid.email AS email_murid",
            "detail_murid.no_telepon AS telepon_murid",
            "guru.name AS nama_guru",
            "detail_guru.photos AS photo_guru"
        )
        ->join(DB::raw("(SELECT `name`,`email`,`id`,`role_id` FROM users WHERE role_id='murid') AS murid "),"murid.id","=","kelas_pesan.siswa_id")
        ->join(DB::raw("(SELECT `name`,`email`,`id`,`role_id` FROM users WHERE role_id='guru') AS guru "),"guru.id","=","kelas_pesan.guru_id")
        ->join(DB::raw("(SELECT * FROM users_detail ) As detail_guru"),"guru.id","=","detail_guru.user_id")
        ->join(DB::raw("(SELECT * FROM users_detail ) As detail_murid"),"murid.id","=","detail_murid.user_id")
        ->where("status_pesan","temp")
        ->where("siswa_id",Auth::user()->id)
        ->first();

       
        return view('checkout',compact('db'));
    }

    public function cart_process(Request $request)
    {
        DB::table("kelas_pesan")->where(["status_pesan"=>"temp","siswa_id"=>Auth::user()->id])->delete();
        $order = "order_".date("YmdHis");
        $request->request->add(['no_order'=> $order]);
        $snapToken = $this->midTransSnap($request);
        $insert = DB::table("kelas_pesan")->insert([
            "guru_id"=>$request->user_id,
            "jam"=>$request->jam,
            "hari"=>$request->hari,
            "kelas"=>$request->kelas,
            "no_order"=> $request->no_order,
            "total_harga"=> $request->harga_guru,
            "siswa_id"=>Auth::user()->id,
            "snap_token"=>$snapToken,
            "status_pesan"=>"temp"
        ]);
        
        return redirect("checkout");
    }

    public function kelas_pesan(Request $request){
        $db = DB::table("kelas_pesan")
            ->select(
                DB::raw("kelas_pesan.*"),
                "users_detail.photos AS photo_guru",
                "murid.name AS nama_murid",
                "guru.name AS nama_guru",
            )
            ->join(DB::raw("(SELECT `name`,`id`,`role_id` FROM users WHERE role_id='murid') AS murid "),"murid.id","=","kelas_pesan.siswa_id")
            ->join(DB::raw("(SELECT `name`,`id`,`role_id` FROM users WHERE role_id='guru') AS guru "),"guru.id","=","kelas_pesan.guru_id")
            ->join("users_detail","guru.id","=","users_detail.user_id")
            ->where("siswa_id",Auth::user()->id)
            ->get();
        return view("kelas_pesan",compact('db'));
    }
}
