<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class PelajaranController extends Controller
{
    public function index()
    {
        $jadwal_pelajaran = DB::table("guru_bidang_studi")
            ->join("guru_jadwal_pelajaran","guru_bidang_studi.bidang_studi","=","guru_jadwal_pelajaran.bidang_studi_id")
            ->where(["guru_bidang_studi.guru_id"=>Auth::user()->id])
            ->get();

        $hari = $this->hari_text();
        return view('pelajaran.index',compact('jadwal_pelajaran','hari'));
    }

    public function tambah_pelajaran(Request $request)
    {
        if($request->submit){
            $pelajaran = DB::table("guru_bidang_studi")
                ->insert([
                    "jenjang"=>$request->jenjang,
                    "kelas"=>$request->kelas,
                    "nama_bidang_studi"=>$request->nama_bidang_studi,
                    "biaya"=>$request->biaya,
                    "guru_id"=>Auth::user()->id,
                    "created_at"=>date("Y-m-d H:i:s"),
                ]);
            
            if($pelajaran){
                $id = DB::getPdo()->lastInsertId();
                $jadwal_hari = $request->jadwal_hari;
                if(is_array($jadwal_hari)){
                    foreach($jadwal_hari AS $key=>$val){
                        DB::table("guru_jadwal_pelajaran")
                            ->insert(
                                [
                                    "guru_id"=>Auth::user()->id,
                                    "bidang_studi_id"=>$id,
                                    "jam_mulai"=>$request->jadwal_waktu_mulai[$key],
                                    "jam_selesai"=>$request->jadwal_waktu_selesai[$key],
                                    "hari"=>$val
                                ]
                            );

                    }
                }
            }
            return redirect("/pelajaran")->with("status_success","Pelajaran Berhasil ditambah");
        }
        return view('pelajaran.tambah');
    }

    public function jadwal_mengajar(Request $request){
        $class_curr = DB::table("kelas_pesan")
            ->select(
                DB::raw("kelas_pesan.*"),
                "murid.name AS nama_murid",
                "guru.name AS nama_guru",
            )
            ->join(DB::raw("(SELECT `name`,`id`,`role_id` FROM users WHERE role_id='murid') AS murid "),"murid.id","=","kelas_pesan.siswa_id")
            ->join(DB::raw("(SELECT `name`,`id`,`role_id` FROM users WHERE role_id='guru') AS guru "),"guru.id","=","kelas_pesan.guru_id")
            ->where("guru_id",Auth::user()->id)
            ->get();
            
        return view('pelajaran.jadwal_mengajar',compact('class_curr'));
    }

    protected function hari_text(){
        $hari = [
           "Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu" 
        ];
        return $hari;
    }
}
