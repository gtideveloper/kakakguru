<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class BillingController extends Controller
{
    public function index(Request $request)
    {
       
        $db = DB::table("guru_billing_info")->where("guru_id",Auth::user()->id)->first();
        if($request->submit){
            DB::table("guru_billing_info")->where("guru_id",Auth::user()->id)->delete();
            $insert_db = DB::table("guru_billing_info")
                ->insert([
                    "no_rekening"=>$request->no_rekening,
                    "guru_id"=>Auth::user()->id,
                    "nama_bank"=>$request->nama_bank,
                    "nama_depan"=>$request->nama_depan,
                    "nama_belakang"=>$request->nama_belakang,
                    "fullname"=>$request->fullname
                ]);
            if($insert_db){
                return redirect()->back()->with("status_success","Data Billing berhasil disimpan");
            }else{
                return redirect()->back()->with("status_failed","Data Billing gagal disimpan");
            }
        }
       
        return view('billing.index',compact('db'));
    }
}
