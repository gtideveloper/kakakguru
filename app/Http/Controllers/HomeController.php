<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;
use Auth;
use DB;
use Storage;
use Mail;
use App\mail\VerificationMail;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $db = DB::table("users_detail")->where("user_id",Auth::user()->id)->first();
        return view('home',compact('db'));
    }

    public function update_profile(Request $request){
        $request->validate([
            'fullname' => 'required|max:255',
            'alamat' => 'required',
            'no_telepon' => 'required',
            'tanggal_lahir' => 'required',
        ]);
        $name = NULL;
        $f_profile = $request->file("photo_profile");
        if(!empty($f_profile)){
            
            $extension = $f_profile->extension();
            $name = "profile_".rand(0000000,9999999999).".".$extension;
            $path = Storage::putFileAs(
                'photo_profile', $f_profile,$name
            );
        }

        $name_ktp = NULL;
        $f_ktp = $request->file("ktp_upload");
        if(!empty($f_ktp)){
            $extension = $f_ktp->extension();

            $name_ktp = "ktp_".rand(00000,9999999999).".".$extension;
            $path = Storage::putFileAs(
                'ktp', $f_ktp,$name_ktp
            );
        }

        DB::table("users_detail")->where("user_id",Auth::user()->id)->delete();
        $insert = DB::table("users_detail")->insert([
            "user_id" => Auth::user()->id,
            "fullname" => $request->fullname,
            "tanggal_lahir" => $request->tanggal_lahir,
            "jenis_kelamin" => $request->jenis_kelamin,
            "alamat" => $request->alamat,
            "no_telepon" => $request->no_telepon,
            "nik" => $request->nik,
            "attachment_ktp" => $name_ktp,
            "photos" => $name,
        ]);

        return redirect("/profil")->with("status_success","Data Profil berhasil disimpan");
    }

    protected $host = "mail.rapatku.com";
    protected $port = "465";
    protected $username = "no-reply@gmail.com";
    protected $password = "kakakguru1234";

    public function sendMessage($sender_address, $sender_name, $email, $subject, $body)
    {
        $transport = (new Swift_SmtpTransport($this->host, $this->port))
            ->setUsername($this->username)
            ->setPassword($this->password);

        $mailer    = new Swift_Mailer($transport);

        $message   = (new Swift_Message($subject))
            ->setFrom($sender_address, $sender_name)
            ->setTo($email)
            ->setBody($body);


        return $mailer->send($message);
    }

    public function galery(Request $request){
        $db = DB::table("users_detail")
            ->select(
                "role_id",
                DB::raw("users_detail.*"),
                "guru_bidang_studi.*"
            )
            ->join("users","users.id","=","users_detail.user_id")
            ->join("guru_bidang_studi","guru_bidang_studi.guru_id","=","users_detail.user_id")
            ->where("role_id","guru");

            if($request->kelas){
                $db  = $db->whereRaw("kelas LIKE '%".$request->kelas."%'");
            }
            if($request->jenjang){
                $db  = $db->whereRaw("jenjang LIKE '%".$request->jenjang."%'");
            }
            if($request->pelajaran){
                $db  = $db->whereRaw("nama_bidang_studi LIKE '%".$request->pelajaran."%'");
            }
        $db = $db->get();

        return view("galery",compact('db'));
    }
    public function update_pendidikan(Request $request){
        DB::table("guru_riwayat_pendidikan")->where("user_id",Auth::user()->id)->delete();
        DB::table("guru_riwayat_pelatihan")->where("guru_id",Auth::user()->id)->delete();
        $name = $request->file("upload_ijazah")->getClientOriginalName();
        //$upload = $request->file("upload_ijazah")->store("public/assets/image_upload");
       
        $path = Storage::putFileAs(
            'ijazah', $request->file('upload_ijazah'),$name
        );
        $insert = DB::table("guru_riwayat_pendidikan")->insert([
            "user_id" => Auth::user()->id,
            "riwayat_pendidikan" => $request->riwayat_pendidikan,
            "upload_ijazah" => $name
        ]);

        $pelatihan_input = $request->pelatihan_input;

        if(is_array($pelatihan_input)){
           
            foreach($pelatihan_input AS $k=>$v){
                $name = $request->file("pelatihan_upload")[$k]->getClientOriginalName();

                $path = Storage::putFileAs(
                    'pelatihan', $request->file('pelatihan_upload')[$k],$name
                );
                $insert = DB::table("guru_riwayat_pelatihan")->insert([
                    "guru_id" => Auth::user()->id,
                    "nama_pelatihan" => $request->riwayat_pendidikan,
                    "attachment_pelatihan" => $name
                ]);            
            }
        }


        return redirect("/profil")->with("status_success","Data Riwayat Guru berhasil disimpan");;
    }
}
