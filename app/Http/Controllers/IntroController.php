<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class IntroController extends Controller
{
    public function index(Request $request){
        $db = DB::table("users_detail")
            ->select(
                "role_id",
                DB::raw("users_detail.*"),
                "guru_bidang_studi.*"
            )
            ->join("users","users.id","=","users_detail.user_id")
            ->join("guru_bidang_studi","guru_bidang_studi.guru_id","=","users_detail.user_id")
            ->where("role_id","guru");
       
            if($request->kelas){
                $db  = $db->whereRaw("kelas LIKE '%".$request->kelas."%'");
            }
            if($request->jenjang){
                $db  = $db->whereRaw("jenjang LIKE '%".$request->jenjang."%'");
            }
            if($request->pelajaran){
                $db  = $db->whereRaw("nama_bidang_studi LIKE '%".$request->pelajaran."%'");
            }
        $db = $db->get();
        return view("intro",compact('db','request'));
    }

    public function gabung(){
        
        return view("gabung");
    }

    public function galery(){
        return view('galery');
    }
}
