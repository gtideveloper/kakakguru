<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class VerificationController extends Controller
{
    public function verified_process($mail_token){
        $check_mail_token = DB::table("users")->where("mail_token",$mail_token)->count();
        if($check_mail_token > 0){
            $update_status = DB::table("users")->where("mail_token",$mail_token)->update(["status_active"=>"active"]);
            if($update_status){
                return redirect("/profil")->with("status_success","Verifikasi Telah Berhasil, Silahkan Lengkapi Data Kamu");
            }else{
                return redirect("/profil")->with("status_failed","Akun Gagal terverifikasi, Silahkan klik link Aktivasi kembali");
            }
        }
        return redirect("/")->with("status_failed","Verifikasi Gagal, Link Aktivasi sudah Kadaluarsa");
    }
}
