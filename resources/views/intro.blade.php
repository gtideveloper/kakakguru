<!DOCTYPE html>
<html>
	<head>
		<title>KakakGuru</title>
		@include("includes.head")
	</head>

	<body >
		@include("includes.header")
		<div class="row p-4">
				<div id="" class="col-md-7">
					<div id="banner" class="carousel slide" data-bs-ride="carousel">
						<div class="carousel-indicators">
					    	<button type="button" data-bs-target="#banner" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
					    	<button type="button" data-bs-target="#banner" data-bs-slide-to="1" aria-label="Slide 2"></button>
					    	<button type="button" data-bs-target="#banner" data-bs-slide-to="2" aria-label="Slide 3"></button>
					  	</div>
					  	<div class="carousel-inner rounded-3">
					    	<div class="carousel-item active">
					      		<img src="{{ url('public/assets/images/banner.gif') }}" class="d-block w-100" alt="...">
					    	</div>
					  	</div>
					  	<button class="carousel-control-prev" type="button" data-bs-target="#banner" data-bs-slide="prev">
					    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    	<span class="visually-hidden">Previous</span>
					  	</button>
					  	<button class="carousel-control-next" type="button" data-bs-target="#banner" data-bs-slide="next">
					    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
					    	<span class="visually-hidden">Next</span>
					  	</button>
					</div>
	        	</div>

				<div id="login" class="col-md-5">
					<div class="container">
						<div class="card">
							<div class="card-header" style="background-color: #42eeee;">
								<div class="card-title">
									<center><p class="bold">Kamu Sebagai Apa?</p></center>
								</div>
							</div>
							<div class="card-body">
								<div class="form-group row pb-2">
									<div class="col-md-6">
										<center>
											<img src="{{ url('public/assets/images/guru1.png')}}" style="height: 100px; width: 100px;" class="pb-2">
											<a href="{{ url('login?s=guru')}}" class="btn btn-sm btn-outline-primary rounded-3" style="font-size: 12px;">Saya Adalah Guru</a>
										</center>
									</div>
									<div class="col-md-6">
										<center>
											<img src="{{ url('public/assets/images/murid1.png')}}" style="height: 100px; width: 100px;" class="pb-2">
											<a href="{{ url('login?s=murid')}}" class="btn btn-sm btn-outline-danger rounded-3" style="font-size: 12px;">Saya Adalah Murid</a>
										</center>
									</div>
								</div>
								<div class="p-2" style="border-radius: 5px; background-color: #ffe;">
									<center>
										<p style="font-style: italic; font-size: 11px;">"Pendidikan adalah kunci keberhasilan dalam hidup, dan guru membuat dampak yang langgeng dalam kehidupan siswa mereka".</p>
										<h6 style="font-weight: bold; font-style: italic; font-size: 12px;">-Solomon Ortiz</h6>
									</center>
								</div>
							</div>
					</div>
					</div>
					
	          	</div>
	          

					<!-- IMAGE SLIDER -->
					<section class="center slider">
						@foreach($db AS $k=>$v)
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-5 " style="border-right:3px solid #DBCFCA">
										<center>
											<img src="{{ url('storage/app/photo_profile/').'/'.$v->photos }}" class="mb-4 bd-placeholder-img rounded-circle" height="90" width="90" role="img">
											<h6>{{ $v->fullname }}</h6>
											<i class="material-icons prefix yellow" >star_border</i>
											<i class="material-icons prefix yellow" >star_border</i>
											<i class="material-icons prefix yellow" >star_border</i>
											<i class="material-icons prefix yellow" >star_border</i>
											<i class="material-icons prefix yellow" >star_border</i>
										</center>
									</div>
									<div class="col-md-7">	
										<div class="bg-default p-3" style="background: #F9F9F9;border-radius:3px;font-size:13px">				
											<p>Bidang Study : {{ $v->nama_bidang_studi }}<br>Tingkatan : {{ $v->jenjang }}<br>Kelas : {{ $v->kelas }}</p>
										</div>
										<br>
										<!-- Button trigger modal -->
										<a href="#" onClick="return showDetail(this,event)" data-id="{{ $v->bidang_studi }}"><button type="button" class="btn btn-info btn-lg" data-bs-toggle="modal" data-bs-target="#viewdetails">View Details</button></a>
										<a href="{{ url('cart?id=').$v->bidang_studi }}"><button type="button" class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#viewdetails">Keranjang</button></a>									
									</div>
								</div>
							</div>
						</div>
						@endforeach
					
					</section>
					@if(!$db->isEmpty())
					<div class="pe-4 d-flex justify-content-end">
						<a href="{{ url('home/galery') }}" class="link-info float-right">Lihat Selengkapnya <i class="fa fa-arrow-right"></i></a>
					</div>
					@endif
					
          	</div>
		 <!-- Modal View Details -->
		 <div class="modal fade" id="viewdetail" tabindex="-1" aria-labelledby="popup-content" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						
					 </div>
					<div class="modal-body">
						<div class="card" style="width: 100%;">
							  <center><img src="contoh1.png" id="img-profil-detail"class="card-img-top" alt="..."></center>
							  <div class="card-body">
								<h5 class="card-title" id="nama-lengkap"></h5>
								{{-- <p class="card-text">"Belajarlah dengan hati yang gembira, karena hati yg gembira adalah Obat semua Masalah."</p> --}}
							  </div>
							<table class="table table-striped">
								<tr>
									<th>
										Pendidikan Terakhir
									</th>
									<td id="riwayat-pendidikan">
										S2 in Harvard University
									</td>
								</tr>
								<tr>
									<th>
										Bidang Study
									</th>
									<td id="nama-bidang-studi">
										
									</td>
								</tr>
								<tr>
									<th>
										Jenjang
									</th>
									<td id="jenjang">
										SMA 1-3
									</td>
								</tr>
							</table>
							
							  <div class="card-body">
								<div class="form-group row" style="padding-top: 10px;">
										<a href="https://www.facebook.com">
											<div class="w-100 py-2 mb-2 btn btn-outline-danger rounded-4" id="email" type="submit">Email : edward@gmail.com
											 </div>
										 </a>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal" style="font-style: italic;">Close</button>
					</div>
				</div>
			</div>
		</div>

	</body>
	<footer>
		<div class="container">
    		<div class="row py-3">
      			<div class="col-4">
        			<img src="{{ url('public/assets/images/akangguru.png') }}" class="bd-placeholder-img rounded-circle" height="50" width="70" role="img">
        			<h5>KakakGuru</h5>
			        <ul class="nav flex-column">
			         	<li class="nav-item mb-2">KakakGuru adalah Aplikasi Edutech yang membantu untuk melakukan pembelajaran lanjutan. KakakGuru didirikan oleh PT GARUDA TEKNOLOGI INDONESIA (GTI)</a></li>
			          	
			        </ul>
      			</div>
      			<div class="col-4">
        			<h5>Section</h5>
			        <ul class="nav flex-column">
			         	<li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Kebijakan & Privasi</a></li>
			          	<li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Syarat & Ketentuan</a></li>
			        </ul>
      			</div>
      			<div class="col-4">
        			<h5>Kontak Kami</h5>
			        <ul class="nav flex-column">
			         	<li class="nav-item mb-2">
			         		<div>
				         		<a href="https://wa.me/message/ZAI53BS4NHCBC1" class="nav-link p-0 text-muted">
				         			<i class="material-icons prefix">whatsapp</i>
				         			0857-7294-4594
				      	   		</a>
				      	   	</div>
			         	</li>
			          	<li class="nav-item mb-2">
			          		<a href="#" class="nav-link p-0 text-muted">
			          			<i class="material-icons prefix">email</i>
			          			info@kakakguru.co.id
			          		</a>
			          	</li>
			        </ul>
			        <h5>Waktu Operasional</h5>
			        <ul class="nav flex-column">
			         	<li class="nav-item mb-2">
			         		<a href="#" class="nav-link p-0 text-muted">
			         			<i class="material-icons prefix">today</i>
			         			Senin - Jum'at
			         		</a>
			         	</li>
			          	<li class="nav-item mb-2">
			          		<a href="#" class="nav-link p-0 text-muted">
			          			<i class="material-icons prefix">watch_later</i>
			          			Pukul 08:00 - 17:00 WIB
			          		</a>
			          	</li>
			        </ul>
      			</div> 
    		</div>

		    <div class="d-flex justify-content-between py-4 my-4 border-top">
		     	<p>&copy; 2021 Company, Inc. All rights reserved.</p>
		      	<ul class="list-unstyled d-flex">
		        	<li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#twitter"/></svg></a></li>
		        	<li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"/></svg></a></li>
		        	<li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"/></svg></a></li>
		      	</ul>
		    </div>
		</div>

	</footer>
	 <!-- SCROLING DOWN -->
	 <script type="text/javascript" >
		$(document).on('click', 'a[href^="#"]', function (event) {
		  event.preventDefault();
	  
		  $('html, body').animate({
			  scrollTop: $($.attr(this, 'href')).offset().top
		  }, 500);
		});
	  </script>
	<script>
		function showDetail(identifier,e){
			e.preventDefault();
			var id = $(identifier).data("id");
			$.ajax({
                url: "{{ url('profil/detail') }}",
                type: "GET",
                dataType : "JSON",
                data:{
					"_token": "{{ csrf_token() }}",
                    "bidang_studi" : id
                },
                success: function(data) {     
					$("#img-profil-detail").attr("src","{{ url('storage/app/photo_profile')}}" + "/" + data.data.photos);
					$("#nama-lengkap").text(data.data.name);
					$("#riwayat-pendidikan").text(data.data.riwayat_pendidikan);
					$("#jenjang").text(data.data.jenjang);
					$("#email").text(data.data.email);
					$("#nama-bidang-studi").text(data.data.nama_bidang_studi);
					$("#viewdetail").modal("show");
                },
                error:function(xhr){
                    console.log(xhr);
                },
                cache: false
            });
		}
		$(".center").slick({
			dots: true,
			infinite: true,
			centerMode: true,
			slidesToShow: 3,
			slidesToScroll: 2,
			autoplay: true,
			autoplaySpeed: 2000
		});
	</script>
	@include("includes.script")
</html>