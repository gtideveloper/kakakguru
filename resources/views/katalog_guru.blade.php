@extends('layouts.app')

@section('content')
<div class="col-lg-10  pb-4">
	<form method="get" class="row mt-4">
						
		<div class="col-md-4">
		   <label for="inputPassword2" class="visually-hidden"></label>
		   <select class="form-select" name="pelajaran" aria-label="Default select example">
			   <option value="">Pilih Pelajaran</option>
			   <option value="matematika">Matematika</option>
			   <option value="bahasa">Bahasa Indonesia</option>
			 </select>
		 </div>
		 <div class="col-md-3">
		   <label for="inputPassword2" class="visually-hidden"></label>
		   <select class="form-select" name="jenjang" aria-label=".form-select-sm example">
			   <option value="">Pilih Jenjang</option>
			   <option value="SD">SD</option>
			   <option value="SMP">SMP</option>
			   <option value="SMA">SMA</option>
		   </select>
		 </div>
		 <div class="col-md-3">
		   <label for="inputPassword2" class="visually-hidden"></label>
		   <select class="form-select" name="kelas" aria-label=".form-select-sm example">
			   <option value="">Pilih Kelas</option>
			   <option value="1">1</option>
			   <option value="2">2</option>
			   <option value="3">3</option>
		   </select>
		 </div>
		 <div class="col-md-2">
		   <button type="submit" class="btn btn-primary mb-3"><i class="fas fa-search"></i> Cari</button>
		 </div>
   
   </form> 	    
	  <div class="row" id="content">
		<center>
		<div class="row mt-3">
			@foreach($db AS $k=>$v)
			<div class="col-lg-3">
				<div class="card">
					<div class="card-body">
	                	<img src="{{ url('storage/app/photo_profile/').'/'.$v->photos }}" class="bd-placeholder-img rounded-circle" height="120" width="140" role="img">
	                   	<h5>{{ $v->fullname }}</h5>
	                    <p>Bidang Study : {{ $v->nama_bidang_studi }}<br>Tingkatan : {{ $v->jenjang }}<br>Kelas : {{ $v->kelas }}</p>
	                    <i class="material-icons prefix yellow" >star_border</i>
	                  	<i class="material-icons prefix yellow" >star_border</i>
	                  	<i class="material-icons prefix yellow" >star_border</i>
	                    <i class="material-icons prefix yellow" >star_border</i>
	                    <i class="material-icons prefix yellow" >star_border</i>
	                    <br>
	                    <!-- Button trigger modal -->
						<a href="#" onClick="return showDetail(this,event)" data-id="{{ $v->bidang_studi }}"><button type="button" class="btn btn-info btn-sm" data-bs-toggle="modal" data-bs-target="#viewdetails">View Details</button></a>
						<a href="{{ url('cart?id=').$v->bidang_studi }}"><button type="button" class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#viewdetails">Keranjang</button></a>
						
	               </div>
        		</div>
        	</div>
			@endforeach		
		</div>
		</center>
			 <!-- Modal View Details -->
			 <div class="modal fade" id="viewdetail" tabindex="-1" aria-labelledby="popup-content" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
						<div class="modal-header">
							
						 </div>
						<div class="modal-body">
							<div class="card" style="width: 100%;">
								  <center><img src="contoh1.png" id="img-profil-detail"class="card-img-top" alt="..."></center>
								  <div class="card-body">
									<h5 class="card-title" id="nama-lengkap"></h5>
									{{-- <p class="card-text">"Belajarlah dengan hati yang gembira, karena hati yg gembira adalah Obat semua Masalah."</p> --}}
								  </div>
								<table class="table table-striped">
									<tr>
										<th>
											Pendidikan Terakhir
										</th>
										<td id="riwayat-pendidikan">
											S2 in Harvard University
										</td>
									</tr>
									<tr>
										<th>
											Bidang Study
										</th>
										<td id="nama-bidang-studi">
											
										</td>
									</tr>
									<tr>
										<th>
											Jenjang
										</th>
										<td id="jenjang">
											SMA 1-3
										</td>
									</tr>
								</table>
								
								  <div class="card-body">
									<div class="form-group row" style="padding-top: 10px;">
											<a href="https://www.facebook.com">
												<div class="w-100 py-2 mb-2 btn btn-outline-danger rounded-4" id="email" type="submit">Email : edward@gmail.com
												 </div>
											 </a>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-bs-dismiss="modal" style="font-style: italic;">Close</button>
						</div>
					</div>
				</div>
			</div>
	
</div>
<script>
	function showDetail(identifier,e){
			e.preventDefault();
			var id = $(identifier).data("id");
			$.ajax({
                url: "{{ url('profil/detail') }}",
                type: "GET",
                dataType : "JSON",
                data:{
					"_token": "{{ csrf_token() }}",
                    "bidang_studi" : id
                },
                success: function(data) {     
					$("#img-profil-detail").attr("src","{{ url('storage/app/photo_profile')}}" + "/" + data.data.photos);
					$("#nama-lengkap").text(data.data.name);
					$("#riwayat-pendidikan").text(data.data.riwayat_pendidikan);
					$("#jenjang").text(data.data.jenjang);
					$("#email").text(data.data.email);
					$("#nama-bidang-studi").text(data.data.nama_bidang_studi);
					$("#viewdetail").modal("show");
                },
                error:function(xhr){
                    console.log(xhr);
                },
                cache: false
            });
		}
</script>
@endsection
