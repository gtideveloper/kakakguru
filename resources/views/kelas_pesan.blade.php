@extends('layouts.app')

@section('content')
<div class="col-lg-10 left">
	
	<div class="col-lg-10">
		<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-2">
		  
		</nav>

		<div class="card mb-2">
			<div class="card-header">
				<div class="card-title">
					<h5>Kelas yang di Ambil</h5>
				</div>
				
			</div>
			<div class="card-body">
				<div class="form-group row">
					
					@foreach ($db AS $v )
								
						<div class="col-md-6">
							<img class="img-fluid" style="width: 440px; height: 300px;" src="{{ url('storage/app/photo_profile/').'/'.$v->photo_guru }}">
						</div>
						<div class="col-md-6">
								<!--<h6 class="" style="font-style: italic;">Hi,Handoko</h6>
								<h4 style="font-weight: bold;">Selamat Datang Dikelas</h4>
								<h6 class="" style="font-style: italic;">Edward Alexander</h6>-->
								<p>Hai, selamat datang di kelas saya! Pelajaran yg kamu ambil adalah {{ $v->kelas }}, ingatlah Matematika Tidak sesulit seperti apa yg kamu bayangkan loh!</p>
								
								

								<div class="form-group row" id="countdown" style="justify-content: center;">
									<h6 class="kelasmulai">Kelas Akan Dimulai Dalam</h6>
									<p id="demo"></p>
								</div>
								<div id="button-mulai" >
									<h6 class="blink" style="font-style: italic; ">Mulai Kelas Sekarang</h6>
									<a href="https://roomgti.daily.co/GSATSYDR172326GDYSSU7DSDG"><button class="btn btn-danger"  style="width: 35%;  font-weight: bold;">Mulai Kelas</button></a>
								</div>
						</div>

						@endforeach
					</div>
			</div>
		</div>
	</div>
	
	<script>
// Set the date we're counting down to
var countDownDate = new Date("Sept 13, 2021 16:31:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    $("#button-mulai").show();
    $("#countdown").hide();
  }
}, 1000);
</script>
@endsection
