@if (Auth::user()->role_id == "guru")
    @include("includes.navbarguru")
@else
    @include("includes.sidebar_admin")
@endif