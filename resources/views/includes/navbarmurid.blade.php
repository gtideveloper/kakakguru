<nav class="navbar navbar-expand-lg navbar-light nvb">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/home') }}"><img src="{{ url('public/assets/images/akangguru.png')}}" style="width: 60px; height: 50px;">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav"  aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="main_nav">
            <ul class="navbar-nav">
                <li class="nav-item active"> <a class="nav-link" href="#">Beranda</a> </li>
                <li class="nav-item"><a class="nav-link" href="#aboutscrolling">Tentang KakakGuru</a></li>
                <li class="nav-item"><a class="nav-link" href="#aboutscrolling">Kontak</a></li>
               
            </ul>

            <ul class="navbar-nav ms-auto">
                <!--NOTOFIKASI-->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-bs-toggle="dropdown" href="#" aria-expanded="true">
                        <i class="far fa-bell fa-2x"></i>
                          <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
                        <span class="dropdown-item dropdown-header">15 Notifications</span>
                          <div class="dropdown-divider"></div>
                          <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                          </a>
                          <div class="dropdown-divider"></div>
                          <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                          </a>
                          <div class="dropdown-divider"></div>
                          <a href="#" class="dropdown-item">
                               <i class="fas fa-file mr-2"></i> 3 new reports
                               <span class="float-right text-muted text-sm">2 days</span>
                         </a>
                          <div class="dropdown-divider"></div>
                          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                  </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" data-bs-toggle="dropdown"><img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle"></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-light text-small shadowd">
                          <li><a class="dropdown-item" href="#">Profil</a></li>
                        <li><a class="dropdown-item" href="#">Pengaturan Akun</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Keluar</a></li>
                    </ul>
                </li>
            </ul>
        </div> <!-- navbar-collapse.// -->
    </div> <!-- container-fluid.// -->
</nav>