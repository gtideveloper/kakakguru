<title>KakakGuru</title>
<link rel="icon" href="{{ url('public/assets/images/teachus.png') }}">
<link rel="stylesheet" type="text/css" href="{{ url('public/assets/plugins/bootstrap/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/slick-theme.css') }}">
		
<link rel="stylesheet" type="text/css" href="https://googleapis.com/css?family=Poppins&display=swap">
<link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/modals/">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="https://kit.fontawesome.com/8eea76d150.js" crossorigin="anonymous"></script>
<script src="{{ url('public/assets/plugins/jquery/jquery-3.6.0.min.js') }}"></script>
<script src="{{ url('public/assets/plugins/bootstrap/js/bootstrap.bundle.js') }}"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<script src="{{ url('public/assets/js/slick.js') }}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-215601711-1">
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-215601711-1');
</script>
	

