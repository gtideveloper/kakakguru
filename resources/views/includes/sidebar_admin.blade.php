            <div class="col-lg-2 sidebars">
		  		<div class="d-flex flex-column flex-shrink-0 p-3  bg-light">
			    	<a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto  text-decoration-none">
			      		<img src="{{ url('public/assets/images/teachus.png') }}" width="40" height="32">
			      		<span class="fs-4">KakakGuru</span>
						
			    	</a>
					<center><h6>(Admin)</h6></center>
					<hr>
		    		<ul class="nav nav-pills flex-column mb-auto">
						<li class="nav-item">
				        	<a href="{{ url('/home') }}" class="nav-link  {{ (Request::is('home')) ? 'active' : '' }}" aria-current="page">
				          		<i class="fas fa-home"></i>
				          		Home
				        	</a>
				      	</li>
						<li>
							<a href="{{ url('admin/master_pelajaran') }}" class="nav-link  {{ (Request::is('master_pelajaran')) ? 'active' : '' }}">
								<i class="fas fa-file"></i>
								Master Pelajaran
							</a>
						</li>
						<li>
							<a href="{{ url('admin/master_jenjang') }}" class="nav-link  {{ (Request::is('master_jenjang')) ? 'active' : '' }}">
								<i class="fas fa-file"></i>
								Master Jenjang
							</a>
						</li>
						<li>
							<a href="{{ url('admin/member') }}" class="nav-link  {{ (Request::is('member')) ? 'active' : '' }}">
								<i class="fas fa-file"></i>
								Member
							</a>
						</li>
						
		    		</ul>
					<hr>
				    <div class="dropdown">
				     	<a href="#" class="d-flex align-items-center  text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
				        	<img src="{{ url('public/assets/images/no-image.png') }}" alt="" width="32" height="32" class="rounded-circle me-2">
				        	<strong>Akun Yg Aktif</strong>
				      	</a>
				      	<ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
				        	<li><a class="dropdown-item" href="{{ url('profil') }}">Profile</a></li>
				        	<li><hr class="dropdown-divider"></li>
				        	<li><a class="dropdown-item" href="{{ url('logout') }}">Sign out</a></li>
				      	</ul>
				    </div>
		  		</div>
			</div>
			