<footer class="mt-auto">
    <div class="container">
        <div class="row py-3">
              <div class="col-4">
                <img src="{{ url('public/assets/images/akangguru.png') }}" class="bd-placeholder-img rounded-circle" height="50" width="70" role="img">
                <h5>KakakGuru</h5>
                <ul class="nav flex-column">
                     <li class="nav-item mb-2">KakakGuru adalah Aplikasi Edutech yang membantu untuk melakukan pembelajaran lanjutan. KakakGuru didirikan oleh PT GARUDA TEKNOLOGI INDONESIA (GTI)</a></li>
                      
                </ul>
              </div>
              <div class="col-4">
                <h5>Section</h5>
                <ul class="nav flex-column">
                     <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Kebijakan & Privasi</a></li>
                      <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Syarat & Ketentuan</a></li>
                </ul>
              </div>
              <div class="col-4">
                <h5>Kontak Kami</h5>
                <ul class="nav flex-column">
                     <li class="nav-item mb-2">
                         <div>
                             <a href="https://wa.me/message/ZAI53BS4NHCBC1" class="nav-link p-0 text-muted">
                                 <i class="material-icons prefix">whatsapp</i>
                                 0857-7294-4594
                                 </a>
                             </div>
                     </li>
                      <li class="nav-item mb-2">
                          <a href="#" class="nav-link p-0 text-muted">
                              <i class="material-icons prefix">email</i>
                              info@kakakguru.co.id
                          </a>
                      </li>
                </ul>
                <h5>Waktu Operasional</h5>
                <ul class="nav flex-column">
                     <li class="nav-item mb-2">
                         <a href="#" class="nav-link p-0 text-muted">
                             <i class="material-icons prefix">today</i>
                             Senin - Jum'at
                         </a>
                     </li>
                      <li class="nav-item mb-2">
                          <a href="#" class="nav-link p-0 text-muted">
                              <i class="material-icons prefix">watch_later</i>
                              Pukul 08:00 - 17:00 WIB
                          </a>
                      </li>
                </ul>
              </div> 
        </div>

        <div class="d-flex justify-content-between py-4 my-4 border-top">
             <p>&copy; 2021 Company, Inc. All rights reserved.</p>
              <ul class="list-unstyled d-flex">
                <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#twitter"/></svg></a></li>
                <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"/></svg></a></li>
                <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"/></svg></a></li>
              </ul>
        </div>
    </div>

</footer>