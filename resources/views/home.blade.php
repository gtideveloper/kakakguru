@extends('layouts.app')

@section('content')

	<div class="modal " id="login1"  tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-scrollable" role="document">
		  <div class="modal-content">
			<div class="modal-header">
			  <h5 class="modal-title">Lengkapi Data Diri</h5>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
                
			<div class="modal-body">
                <form action="{{ url('update_profile') }}" method="POST" enctype="multipart/form-data">            
				<div class="container popup mb-3">
					<center>
                        <ul class="progressbar">
                            <li class="satu">Identitas</li>
                            @if(Auth::user()->role_id == "guru")
                             {{-- <li class="">Riwayat Pendidikan</li> --}}
                            @endif
                        </ul>
                    </center>
				</div>
				<div>
				       @csrf()
						<div class="form-group">
							<div class="col-sm-4">
								<label>Nama Lengkap :</label>
							</div>
                    		<input type="text" placeholder="Nama Lengkap" required value="{{ Auth::user()->name }}" name="fullname" id="nama-lengkap" class="form-control rounded-pill">
						</div><br/>
						<div class="form-group">
                            <div class="col-sm-4">
								<label>Alamat :</label>
							</div>
							<input type="text" placeholder="Alamat" id="alamat" required name="alamat" class="form-control rounded-pill">
						</div><br>
						<div class="form-group">
                            <div class="col-sm-4">
								<label>Nik :</label>
							</div>
							<input type="text" placeholder="NIK" id="nik" name="nik" required class="form-control rounded-pill">
						</div><br/>
                        <div class="form-group">
                            <div class="col-sm-4">
								<label>Jenis Kelamin :</label>
							</div>
							<select class="form-control" required name="jenis_kelamin">
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                            </select>
						</div><br/>
						<div class="form-group row">
							<div class="col-sm-4">
								<p>Tanggal Lahir :</p>
							</div>
							<div class="col-sm-8">
								<input type="date" name="tanggal_lahir" required id="tanggal-lahir" class="form-control rounded-pill">
							</div>
						</div><br/>
                        
						<div class="form-group">
                            <div class="col-sm-4">
								<label>No Telepon :</label>
							</div>
							<input type="text" placeholder="No Telepon"  required name="no_telepon" id="no-telepon" class="form-control rounded-pill">
						</div>	<br/>
                        <div class="form-group">
                            <div class="col-sm-4">
								<label>Upload :</label>
							</div>
                            <input type="hidden" name="photos_hidden" id="photos_hidden"/>
							<input type="file" onchange="loadFile(this,event)" data-target="profil-img" name="profile_upload" id="profile_upload" class="form-control rounded-pill mb-3">
                            <img class="rounded-pill" id="profil-img" style="border:1px solid black;width:100px;height:100px" src="{{ url('public/assets/images/no-image.png') }}" alt="no-image"/>
						</div>	
				</div>

			</div>
			<div class="modal-footer">
			    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			    @if(Auth::user()->role_id == "guru")
                    <button type="button" onClick="return stepData1()"  class="btn btn-primary">Next</button>
                @else
                    <button type="submit"   class="btn btn-primary">Save</button>
                @endif
            </div>
            </form>
		  </div>
		</div>
	</div>	

	<!-- Modal -->
	<div class="modal fade" id="login2" tabindex="-1" aria-labelledby="popup-content" aria-hidden="true" style="display:none">
		<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="popup-content">Lengkapi Data Diri</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			  </div>

			  <div class="modal-body">
					<div class="container popup">
						<ul class="progressbar">
							<li class="">Identitas</li>
							<li class="dua">Kontak</li>
							<li class="">Selesai</li>
						</ul>
					</div>
					<form name="form" method="post" enctype="multipart/form-data"> 
						
						<div class="form-group">
							<label for="no-telepon">Riwayat Pendidikan</label>
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Jenjang pendidikan Terakhir</th>
										<th>Upload Poto Ijazah Terakhir</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><input type="text" class="form-control" name="riwayat_input" id="riwayat_input"/></td>
										<td>
											<div class="custom-file">
												<label class="custom-file-label" for="customFileLang">Upload File</label>
												<input type="file" onchange="loadFile(this,event)" data-target="riwayat-img" class="custom-file-input" name="riwayat_upload" id="riwayat_upload">
										  	</div>
										</td>
										<td class="d-flex justify-content-center"><img id="riwayat-img" src="{{ url('public/assets/images/no-image.png') }}" style="width:60px;height:60px" /></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="form-group">
							<label for="formFileSm" class="form-label">Upload Foto KTP</label>
							<div class="row">
								<div class="col-md-10">
									<div class="custom-file">
										<label class="custom-file-label" for="customFileLang">Upload File</label>
										<input type="file" onchange="loadFile(this,event)" data-target="ktp-img" class="custom-file-input"  name="ktp_upload" id="ktp_upload">
									</div>
								</div>
								<div class="col-md-2 mb-1">
									<p><img id="ktp-img" src="{{ url('public/assets/images/no-image.png') }}" style="width:60px;height:60px" /></p>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="formFileSm" class="form-label">Riwayat Pelatihan</label>
							<table class="table table-bordered table-striped" id="sample_table">
								<thead>
									<tr>
										<th>Nama Pelatihan</th>
										<th>Upload Poto Sertifikat Pelatihan</th>
										<th>
											<button class="btn btn-success btn-sm" onClick="return addRow()" type="button"><i class="fas fa-plus text-white"></i></button>
											<button class="btn btn-danger btn-sm" onClick="return removeRow()" type="button"><i class="fas fa-minus text-white"></i></button>
										</th>
									</tr>
								</thead>
								<tbody class="riwayat-tbody">
									<tr class="row-content">
										<td><input type="text" class="form-control" name="pelatihan_input[]"/></td>
										<td>
											<div class="custom-file">
												<label class="custom-file-label" >Upload File</label>
												<input type="file" onchange="loadFile(this,event)" data-target="riwayat-img" name="pelatihan_upload[]" id="pelatihan_upload_0" onchange="loadFile(this,event)" data-target="pelatihan-img" class="custom-file-input" id="customFileLang" lang="es">
										  	</div>
										</td>
										<td class="d-flex justify-content-center"><img id="pelatihan-img" src="{{ url('public/assets/images/no-image.png') }}" style="width:60px;height:60px" /></td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
				  </div>

				<div class="modal-footer">
					<button type="button" class="btn btn-warning"  onClick="return stepData0()"  >Back</button>
					<button type="button" class="btn btn-secondary"    data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary"  onClick="return stepData2()" >Save changes</button>
				</div>
			</div>
		  </div>						 
	</div>
    
    <script>
        
        function stepData1(){
			$("#login1").modal("hide");
			$("#login2").modal({backdrop: 'static', keyboard: false});
			let formData = new FormData();

			formData.append('nama_lengkap',$("#nama-lengkap").val());
			formData.append('alamat',$("#alamat").val());
			formData.append('_token',"{{ csrf_token() }}");
			formData.append('nik',$("#nik").val());
			formData.append('jenis_kelamin',$("#jenis_kelamin").find("option:selected").val());
			formData.append('tanggal_lahir',$("#tanggal_lahir").val());
		}

        function saveProcess(){
            let formData = new FormData();

            let profile_upload  = document.getElementById('profile_upload').files[0];

            formData.append('nama_lengkap',$("#nama-lengkap").val());
			formData.append('alamat',$("#alamat").val());
			formData.append('nik',$("#nik").val());
			formData.append('jenis_kelamin',$("#jenis_kelamin").find("option:selected").val());
			formData.append('tanggal_lahir',$("#tanggal_lahir").val());
			formData.append('profile_upload',profile_upload);

            console.log(formData);


			let upload = (data) => {
				fetch('{{ url("/update_profile") }}', { // Your POST endpoint
					method: 'POST',
					cache: 'no-cache', 
					body: data // This is your file object
				}).then(
					response => response.json()// if the response is a JSON object
				).then(
					success => {
						Swal.fire(
							'Berhasil!',
							'Data Berhasil disimpan!',
							'success'
						)
					} // Handle the success response object
				).catch(
					error => console.log(error) // Handle the error response object
				);
			};

			upload(formData);
        }
		
		function stepData0(){
			$("#login2").modal("hide");
			$("#login1").modal({backdrop: 'static', keyboard: false});
		}

		function stepData2(){
			//$("#login2").modal("hide");
			
			let riwayat_upload  = document.getElementById('riwayat_upload').files[0];
			//let pelatihan_upload  = document.getElementById('pelatihan_upload').files[0];
			let ktp_upload  = document.getElementById('ktp_upload').files[0];
			let row_pelatihan = document.getElementsByClassName('row-content').length;
			$("input[name^='pelatihan_input']").each(function(index,value){
				formData.append("pelatihan_input_" + index,$(this).val());
			});

			$("input[name^='pelatihan_upload']").each(function(index,value){
				let id = $(this).attr("id");
				formData.append("pelatihan_upload_" + index,document.getElementById(id).files[0]);
			});
			
			formData.append('ktp_upload', ktp_upload);
			formData.append('riwayat_input', $("#riwayat_input").val());
			formData.append('riwayat_upload', riwayat_upload);
			
			let upload = (data) => {
				fetch('/update_profile', { // Your POST endpoint
					method: 'POST',
					cache: 'no-cache', 
					body: data // This is your file object
				}).then(
					response => response.json()// if the response is a JSON object
				).then(
					success => {
						Swal.fire(
							'Berhasil!',
							'Data Berhasil disimpan!',
							'success'
						)
					} // Handle the success response object
				).catch(
					error => console.log(error) // Handle the error response object
				);
			};

			upload(formData);
		}

		function addRow(){
			let content = $('.row-content:last'),
			size = $('#sample_table > tbody >tr').length + 1;
			let element = null;
			element = content.clone();
			element.attr('id', 'rec-'+size);
			element.appendTo('.riwayat-tbody');
			element.find('input[type="file"]').attr("id","pelatihan_upload_"+size);
			element.find('input[type="file"]').attr("data-target","pelatihan-img-"+size);
			element.find('img').attr("id","pelatihan-img-"+size);
		}

		let loadFile = function(identifier,event) {
			let target = $(identifier).data("target");
			let reader = new FileReader();
			
			reader.onload = function(){
				let output = document.getElementById(target);
                $("#photos_hidden").val(reader.result);
				output.src = reader.result;
			};
			reader.readAsDataURL(event.target.files[0]);
		};
        $(document).ready(function(){
			@if(empty($db->nik))
            	$("#login1").modal({backdrop: 'static', keyboard: false});
			@endif
	    });
    </script>







@endsection
