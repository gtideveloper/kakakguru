@extends('layouts.app')

@section('content')

<div class="col-lg-10">
    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">
                <h5>Detail Profil</h5>
            </div>
            
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <center>
                        <img src="{{ url('storage/app/photo_profile/').'/'.$db->photos }}" style="width:200px;height:200px" class="mb-4 rounded-circle">
                        <div class="detailrating">
                            <i class="material-icons prefix yellow" >star_border</i>
                            <i class="material-icons prefix yellow" >star_border</i>
                            <i class="material-icons prefix yellow" >star_border</i>
                            <i class="material-icons prefix yellow" >star_border</i>
                            <i class="material-icons prefix yellow" >star_border</i>
                        </div>
                    </center>
                </div>

                <div class="col-md-8">
                    <table class="table table-bordered border-secondary">
                        <tr>
                            <th>
                                Nama Lengkap 
                            </th>
                            <td>
                                {{ $db->fullname }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Pendidikan Terakhir 
                            </th>
                            <td>
                                S1 Gunadarma
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Bidang Study 
                            </th>
                            <td>
                               {{ $db->nama_bidang_studi }}
                            </td>
                        </tr>
                        
                        <tr>
                            <th>
                                Jenjang 
                            </th>
                            <td>
                                {{ $db->jenjang }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Kelas 
                            </th>
                            <td>
                                {{ $db->kelas }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">
                <div class="row">
                    <div class="col-md-5">
                        <h5>Detail Pemesanan</h5>
                    </div>
                    <div class="col-md-7">
                        <h5>Detail Pembayaran</h5>
                    </div>
                </div>
            </div>
            
        </div>
        <form action="{{ url('cart_process') }}" method="POST" >
            @csrf 
            <div class="card-body">
                <div class="row">
                    <div class="col-md-5">
                        <table class="table table-bordered border-secondary">
                                    <tr>
                                        <th>
                                            Kelas Yang Diambil
                                        </th>
                                        <td>
                                            <div class="form-group">
                                                <input type="hidden" name="user_id" value="{{ $db->user_id }}"/>
                                                <input type="text" name="kelas" class="form-control" value="{{ $db->nama_bidang_studi." ".$db->jenjang." Kelas ".$db->kelas }}" readonly/>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Jadwal Kelas
                                        </th>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-select border-danger" name="hari">
                                                    @foreach($guru_jadwal AS $v)
                                                        <option value="{{ $hari[$v->hari] }}">{{ $hari[$v->hari] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <th>
                                            Pilih Jam Kelas
                                        </th>
                                        <td>
                                            <div class="form-group border-danger">
                                                <select class="form-select border-danger" name="jam">
                                                    @foreach($guru_jadwal AS $v)
                                                        <option value="{{ $v->jam_mulai." - ".$v->jam_selesai   }}">{{ $v->jam_mulai." - ".$v->jam_selesai." WIB"  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                    </div>

                    <div class="col-md-7">
                        <table class="table table-bordered border-secondary">
                                    <tr>
                                        <th>
                                            Total Harga 
                                        </th>
                                        <td>
                                            <input type="hidden" value="{{ $db->biaya  }}" name="harga_guru"/>
                                            Rp. {{ $db->biaya  }}
                                        </td>
                                    </tr>
                                    {{-- <tr>
                                        <th>
                                            Pilih Pembayaran
                                        </th>
                                        <td>
                                            <div class="form-group">
                                                <select id="jk" class="form-control border-danger">
                                                    <option value="shopeepay">Shopeepay</option>
                                                    <option value="Gopay">Gopay</option>
                                                </select> 
                                            </div>
                                        </td>
                                    </tr> --}}
                                </table>
                                <button type="submit" class="btn btn-danger btn-sm"  id="pay-button">
                                    BUY NOW!! <i class="fas fa-shopping-cart"></i>
                                </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection