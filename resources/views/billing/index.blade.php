@extends('layouts.app')

@section('content')
<div class="col-lg-10">
	<div class="card mb-2">
        <div class="card-header">
            <div class="card-title">
                <h5>Informasi Billing</h5>
            </div>
            
        </div>
        <div class="card-body">
            <div id="billing">
                <div class="form-group row">
                    {{-- <center><p class="bold">Pilih Jenis Transaksi yang diinginkan</p></center> --}}
                    {{-- <hr> --}}
                    <div class="col-md-12">
                        <center><i class="fas fa-university" style="color: black; font-size: 50px;"></i></center>
                        <p class="my-4 text-center small"><a href="billing2.html"><button class="btn btn-primary rounded-pill">Bank Transfer</button></a></p>
                    </div>
                    {{-- <div class="col-md-6">
                        <center><i class="fas fa-wallet" style="color: black; font-size: 50px;"></i></center>
                        <p class="my-4 text-center small"><a href="billing.html"><button class="btn btn-primary rounded-pill">E-Wallet</button></a></p>
                    </div> --}}
                </div>
          </div>
        </div>
    </div>
    <form method="post">
        @csrf
    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">
                <h5>Pilih Jenis Bank</h5>
            </div>
        </div>

        <div class="card-body">
            <div id="tabs">
                <div class="form-group row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <select id="nama_bank" name="nama_bank" class="form-select border-primary">
                                <option value="bca" {{ (!empty($db->nama_bank) && $db->nama_bank=="bca"  ? "selected" : "") }}>BCA (Bank Central Asia)</option>
                                <option value="bri" {{ (!empty($db->nama_bank) && $db->nama_bank=="bri"  ? "selected" : "") }}>BRI (Bank Rakyat Indonesia)</option>
                                <option value="bni" {{ (!empty($db->nama_bank) && $db->nama_bank=="bni"  ? "selected" : "") }}>BNI (Bank Negara Indonesia)</option>
                                <option value="mandiri" {{ (!empty($db->nama_bank) && $db->nama_bank=="mandiri"  ? "selected" : "") }}>MANDIRI</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-7">
                        <input type="number" placeholder="Nomor Rekening" name="no_rekening"  value="{{ (!empty($db->no_rekening)  ? $db->no_rekening : "") }}" class="form-control rounded-pill">
                    </div>
                </div>
              </div>
        </div>
    </div>

    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">
                <h5>Informasi Pemilik Rekening</h5>
            </div>
        </div>
        
            <div class="card-body">
                   
                    <div id="tabs2">
                        <div class="form-group row">
                           
                                
                                <div class="mb-3 form-group row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Nama Depan" name="nama_depan" value="{{ (!empty($db->nama_depan)  ? $db->nama_depan : "") }}" class="form-control rounded-pill">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Nama Belakang" name="nama_belakang" value="{{ (!empty($db->nama_belakang)  ? $db->nama_belakang : "") }}" class="form-control rounded-pill">
                                    </div>
                                </div>
                                
                                <div class="mb-3 form-group">
                                        <input type="text" placeholder="Nama yang terdaftar di Rekening" name="fullname"  value="{{ (!empty($db->fullname)  ? $db->fullname : "") }}" class="form-control rounded-pill">
                                </div>
                                <div class="mb-3 form-group">
                                    <input type="text" placeholder="Email Aktif" value="{{ Auth::user()->email }}" readonly class="form-control rounded-pill">
                                </div>
                                    {{-- <div class="form-group">
                                        <div class="costum-control">
                                            <input type="checkbox" id="CostumCheckbox1" class="costum-control-input">
                                            <label class="costum-control-label"for="CostumCheckbox1">Saya Setuju&Telah mengisi data dengan benar</label>
                                        </div>
                                    </div> --}}
                                <br>
                           
                                <div class="form-group">
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary rounded-pill" style="width: 100%;">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
      
    </div>
</div>	
@endsection
