@extends('layouts.app')

@section('content')
    <div class="col-lg-10">
        @if (Auth::user()->status_active == "nonactive")     
            <div class="alert alert-danger">
                Pendaftaran telah Berhasil dilakukan. Silahkan Cek Email kamu di folder Inbox atau Spam dan klik "Link Aktivasi" Untuk mengaktifkan Akun Kamu.
            </div>
        @endif
        <div class="progressbar-group mb-3">
            <ul class="progressbar">
                <li class="active">Step 1</li>
                <li>Step 2</li>
                <li>Step 3</li>
            </ul>

        </div>

        <form action="{{ url('update_profile') }}" method="POST" enctype="multipart/form-data">   
            @csrf     
            <div class="card" id="biodata">
                <div class="card-header">
                    <div class="card-title">Lengkapi Data Diri</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label class="required">Nama Lengkap</label>
                        </div>
                        <input type="text" placeholder="Nama Lengkap" required value="{{ Auth::user()->name }}" name="fullname" id="nama-lengkap" class="form-control rounded-pill @error('fullname') is-invalid @enderror">
                        @error('fullname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div><br/>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label class="required">Alamat</label>
                        </div>
                        <input type="text" value="{{ (!empty($db->alamat) ? $db->alamat : '' ) }}"placeholder="Alamat" id="alamat" required name="alamat" class="form-control rounded-pill  @error('alamat') is-invalid @enderror">
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div><br>
                    @if (Auth::user()->role_id == "guru")
                        
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label class="required">Nik</label>
                        </div>
                        <input type="text" placeholder="NIK" value="{{ (!empty($db->nik) ? $db->nik : '' ) }}"  id="nik" name="nik" required class="form-control rounded-pill @error('nik') is-invalid @enderror">
                        @error('nik')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div><br/>
                    <div class="form-group">
                        <label for="formFileSm" class="form-label">Upload Foto KTP</label>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="custom-file">
                                    <input type="file" onchange="loadFile(this,event)" data-target="ktp-img" class="custom-file-input @error('ktp_upload') is-invalid @enderror"  name="ktp_upload" id="ktp_upload">
                                </div>
                            </div>
                            <div class="col-md-2 mb-1">
                                <p><img id="ktp-img" src="{{ (!empty($db->photos) ? url('storage/app/ktp/').'/'.$db->attachment_ktp : url('public/assets/images/no-image.png') ) }} style="width:60px;height:60px" /></p>
                            </div>
                        </div>
                    </div>
       
                    @endif
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label class="required">Jenis Kelamin</label>
                        </div>
                        <select class="form-control @error('jenis_kelamin') is-invalid @enderror" required name="jenis_kelamin">
                            <option value="L" {{ (!empty($db->jenis_kelamin) && $db->jenis_kelamin == "L"  ? 'selected' : '' ) }}>Laki-laki</option>
                            <option value="P" {{ (!empty($db->jenis_kelamin) && $db->jenis_kelamin == "P"  ? 'selected' : '' ) }}>Perempuan</option>
                        </select>
                        @error('jenis_kelamin')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div><br/>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <p class="required">Tanggal Lahir</p>
                        </div>
                        <div class="col-sm-8">
                            <input type="date" value="{{ (!empty($db->tanggal_lahir) ? $db->tanggal_lahir : '' ) }}" name="tanggal_lahir" required id="tanggal-lahir" class="form-control rounded-pill @error('tanggal_lahir') is-invalid @enderror">
                        </div>
                        @error('tanggal_lahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div><br/>
                    
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label>No Telepon :</label>
                        </div>
                        <input type="text" placeholder="No Telepon"  value="{{ (!empty($db->no_telepon) ? $db->no_telepon : '' ) }}" required name="no_telepon" id="no-telepon" class="form-control rounded-pill @error('no_telepon') is-invalid @enderror">
                        @error('no_telepon')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div><br/>
                    @if (Auth::user()->role_id == "guru")
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label>Upload Poto Profile</label>
                        </div>
                        <input type="file" onchange="return loadFile(this,event)"  data-target="profil-img" name="photo_profile" id="photo_profile" class="form-control rounded-pill mb-3">
                        <img class="rounded-pill" id="profil-img" style="border:1px solid black;width:100px;height:100px" src="{{ (!empty($db->photos) ? url('storage/app/photo_profile/').'/'.$db->photos : url('public/assets/images/no-image.png') ) }}" alt="no-image"/>
                    </div>	 
                    @endif   
                </div>
                @if (Auth::user()->status_active == "active")
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info" >Simpan</button>
                    </div>	
                @endif
                
            </div><br/>
        </form>
        @if (Auth::user()->role_id == "guru")
        <form action="{{ url('update_pendidikan') }}" method="POST" enctype="multipart/form-data">   
            @csrf
            <div class="card">        
                <div class="card-header">
                <h5 class="card-title" id="popup-content">Riwayat Pendidikan</h5>
                </div>   
                <div class="card-body">                
                    <div class="form-group">
                        <label for="no-telepon">Riwayat Pendidikan</label>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="required">Jenjang pendidikan Terakhir</th>
                                    <th class="required">Upload Poto Ijazah Terakhir (*pdf)</th>
                                    <th>Link PDF</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" value="{{ (!empty($db_pendidikan->riwayat_pendidikan) ? $db_pendidikan->riwayat_pendidikan : '') }}" class="form-control" required name="riwayat_pendidikan" id="riwayat_pendidikan"/></td>
                                    <td>
                                        <div class="custom-file">
                                            <label class="custom-file-label"  for="customFileLang">Upload File</label>
                                            <input type="file"  required class="custom-file-input" name="upload_ijazah" id="upload_ijazah">
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-center"><a href="{{ (!empty($db_pendidikan->upload_ijazah) ? url('storage/app/ijazah').'/'.$db_pendidikan->upload_ijazah : '#') }}" >Pdf</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <label for="formFileSm" class="form-label">Riwayat Pelatihan</label>
                        <table class="table table-bordered table-striped" id="sample_table">
                            <thead>
                                <tr>
                                    <th>Nama Pelatihan</th>
                                    <th>Upload Poto Sertifikat Pelatihan</th>
                                    <th>
                                        <button class="btn btn-success btn-sm" onClick="return addRow()" type="button"><i class="fas fa-plus text-white"></i></button>
                                        <button class="btn btn-danger btn-sm" onClick="return removeRow()" type="button"><i class="fas fa-minus text-white"></i></button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="riwayat-tbody">

                                @if ($db_pelatihan_num > 0)
                                    @foreach ($db_pelatihan as $k=>$v)
                                        <tr class="row-content">
                                            <td><input type="text" class="form-control" value="{{ (!empty($v->nama_pelatihan) ? $v->nama_pelatihan : '') }}" name="pelatihan_input[]" value/></td>
                                            <td>
                                                <div class="custom-file">
                                                    <label class="custom-file-label" >Upload File</label>
                                                    <input type="file" name="pelatihan_upload[]" id="pelatihan_upload_0" onchange="loadFile(this,event)" data-target="pelatihan-img" class="custom-file-input" id="customFileLang" lang="es">
                                                </div>
                                            </td>
                                            <td class="d-flex justify-content-center"><a href="{{ (!empty($v->attachment_pelatihan) ? url('storage/app/ijazah').'/'.$v->attachment_pelatihan : '#') }}" >Pdf</a></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr class="row-content">
                                        <td><input type="text" class="form-control" name="pelatihan_input[]"/></td>
                                        <td>
                                            <div class="custom-file">
                                                <label class="custom-file-label" >Upload File</label>
                                                <input type="file" name="pelatihan_upload[]" id="pelatihan_upload_0" onchange="loadFile(this,event)" data-target="pelatihan-img" class="custom-file-input" id="customFileLang" lang="es">
                                            </div>
                                        </td>
                                        <td class="d-flex justify-content-center"></td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                @if (Auth::user()->status_active == "active")
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info" >Simpan</button>
                    </div>	
                @endif
               		 
            </div>
        </form>
        @endif
    
    </div>
    <script>
        let loadFile = function(identifier,event) {
			let target = $(identifier).data("target");
			let reader = new FileReader();
			
			reader.onload = function(){
				let output = document.getElementById(target);
                $("#photos_hidden").val(reader.result);
				output.src = reader.result;
			};
			reader.readAsDataURL(event.target.files[0]);
		};

        function addRow(){
			let content = $('.row-content:last'),
			size = $('#sample_table > tbody >tr').length + 1;
			let element = null;
			element = content.clone();
			element.attr('id', 'rec-'+size);
			element.appendTo('.riwayat-tbody');
			element.find('input[type="file"]').attr("id","pelatihan_upload_"+size);
			element.find('input[type="file"]').attr("data-target","pelatihan-img-"+size);
			element.find('img').attr("id","pelatihan-img-"+size);
		}
        $(document).ready(function(){
            @if(Auth::user()->status_active == 'nonactive')
                $('input,select').attr("disabled",true);
            @endif
           
	    });
    </script>
@endsection
