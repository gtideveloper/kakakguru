@extends('layouts.app')

@section('content')
<div class="col-lg-10">
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-2">
      
    </nav>


    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">
                <h5> DETAIL PEMESAN</h5>
            </div>
        </div>

        <div class="card-body">
            <table class="table table-striped" style="text-align: center;">
                <thead class="table-dark">
                    <tr>
                        <th>
                            Nama Murid 
                        </th>
                        <th>
                            Email 
                        </th>
                        <th>
                            No. Handphone 
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            {{ $db->nama_murid }}
                        </td>
                        <td>
                            {{ $db->email_murid }}
                        </td>
                        <td>
                            {{ $db->telepon_murid }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="card mb-2">
        <div class="card-header">
            <div class="card-title">
                <h5>DETAIL PEMBELIAN</h5>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <center><img src="{{ url('storage/app/photo_profile/').'/'.$db->photo_guru }}" style="width:300px;height:250px"></center>
                        
                </div>

                <div class="col-md-8">
                    <div class="card mb-2">
                        <div class="card-body">
                        <!--Detail Nama-->
                            <h1>{{ $db->nama_guru }}</h1>
                            <div class="row">
                                <div class="detailrating">
                                    <i class="material-icons prefix yellow" >star_border</i>
                                    <i class="material-icons prefix yellow" >star_border</i>
                                    <i class="material-icons prefix yellow" >star_border</i>
                                    <i class="material-icons prefix yellow" >star_border</i>
                                    <i class="material-icons prefix yellow" >star_border</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card">
                        <!-- PRICE LIST -->
                        <div class="card-body">
                            <div class="detailpembelian">
                                <div class="">
                                    <table class="table table-striped" style="text-align: center;">
                                        <thead class="table-dark">
                                            <tr>
                                            <th>
                                                 Pelajaran 
                                            </th>
                                            <th>
                                                Hari
                                            </th>
                                            <th>
                                                Jam Kelas 
                                            </th>
                                            <th>
                                                Total Harga
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    {{ $db->kelas }} 
                                                </td>
                                                <td>
                                                    {{ $db->hari }} 
                                                </td>
                                                <td>
                                                    {{ $db->jam }}  WIB
                                                </td>
                                                <td>
                                                    Rp. {{ number_format($db->total_harga) }}
                                                </td>
                                              </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	

    <div class="row">
        
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered border-secondary">
                        <tr>
                            <th>
                                Total Harga
                            </th>
                            <td>
                                <center>
                                    <span style="font-weight: bold; font-style: italic;">Rp {{ $db->total_harga }}</span>
                                </center>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>	
        </div>
        <div class="col-md-3 mt-4">				
            <center>
                <button type="button" id="pay-button" class="btn btn-danger" >
                    Bayar Sekarang <i class="fas fa-shopping-cart"></i>
                </button>
            </center>
        </div>
        
    </div>

</div>
<script type="text/javascript"
    src="https://app.midtrans.com/snap/snap.js"
    data-client-key="Mid-client-UmYYDguBhX0qgSnW"></script>
<script type="text/javascript">
 
    var payButton = document.getElementById('pay-button');
    // For example trigger on button clicked, or any time you need
    payButton.addEventListener('click', function () {
        window.snap.pay('{{ $db->snap_token }}', {
                onSuccess: function(result){
                    console.log('success');console.log(result);
                    console.log(result.status_code);            
                },
                onPending: function(result){
                    document.location = "{{ url('kelas_pesan') }}"
                    console.log(result.status_code);
                },
                onError: function(result){console.log('error');console.log(result);},
                onClose: function(){console.log('customer closed the popup without finishing the payment');}
            })
    });
</script>
@endsection