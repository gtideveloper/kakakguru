<!DOCTYPE html>
<html>
	<head>
		<title>KakakGuru</title>
		@include("includes.head")
	</head>

	<body class="d-flex flex-column min-vh-100"> 
		@include("includes.header")
		<form method="get" class="row p-3">
						
			<div class="col-md-4">
			   <label for="inputPassword2" class="visually-hidden"></label>
			   <select class="form-select" name="pelajaran">
				   <option value="">Pilih Pelajaran</option>
				   <option value="matematika">Matematika</option>
				   <option value="bahasa">Bahasa Indonesia</option>
				 </select>
			 </div>
			 <div class="col-md-3">
			   <label for="inputPassword2" class="visually-hidden"></label>
			   <select class="form-select" name="jenjang">
				   <option value="">Pilih Jenjang</option>
				   <option value="SD">SD</option>
				   <option value="SMP">SMP</option>
				   <option value="SMA">SMA</option>
			   </select>
			 </div>
			 <div class="col-md-3">
			   <label for="inputPassword2" class="visually-hidden"></label>
			   <select class="form-select" name="kelas">
				   <option value="">Pilih Kelas</option>
				   <option value="1">1</option>
				   <option value="2">2</option>
				   <option value="3">3</option>
			   </select>
			 </div>
			 <div class="col-md-2">
			   <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Cari</button>
			 </div>
	   
	   </form> 	   
		<div class="row  p-3">
			@foreach($db AS $k=>$v)
			<div class="col-md-4">
		
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-5 " style="border-right:3px solid #DBCFCA">
								<center>
									<img src="{{ url('storage/app/photo_profile/').'/'.$v->photos }}" class="mb-4 bd-placeholder-img rounded-circle" height="90" width="90" role="img">
									<h6>{{ $v->fullname }}</h6>
									<i class="material-icons prefix yellow" >star_border</i>
									<i class="material-icons prefix yellow" >star_border</i>
									<i class="material-icons prefix yellow" >star_border</i>
									<i class="material-icons prefix yellow" >star_border</i>
									<i class="material-icons prefix yellow" >star_border</i>
								</center>
							</div>
							<div class="col-md-7">	
								<div class="bg-default p-3" style="background: #F9F9F9;border-radius:3px;font-size:13px">				
									<p>Bidang Study : {{ $v->nama_bidang_studi }}<br>Tingkatan : {{ $v->jenjang }}<br>Kelas : {{ $v->kelas }}</p>
								</div>
								<br>
										<!-- Button trigger modal -->
								<a href="#" onClick="return showDetail(this,event)" data-id="{{ $v->bidang_studi }}"><button type="button" class="btn btn-info btn-lg" data-bs-toggle="modal" data-bs-target="#viewdetails">View Details</button></a>
								<a href="{{ url('gabung') }}"><button type="button" class="btn btn-warning btn-sm">Gabung</button></a>									
							</div>
						</div>
					</div>
				</div>
			</div>
		@endforeach
		</div>
	 <!-- Modal View Details -->
	 <div class="modal fade" id="viewdetail" tabindex="-1" aria-labelledby="popup-content" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					
				 </div>
				<div class="modal-body">
					<div class="card" style="width: 100%;">
						  <center><img src="contoh1.png" id="img-profil-detail"class="card-img-top" alt="..."></center>
						  <div class="card-body">
							<h5 class="card-title" id="nama-lengkap"></h5>
							{{-- <p class="card-text">"Belajarlah dengan hati yang gembira, karena hati yg gembira adalah Obat semua Masalah."</p> --}}
						  </div>
						<table class="table table-striped">
							<tr>
								<th>
									Pendidikan Terakhir
								</th>
								<td id="riwayat-pendidikan">
									S2 in Harvard University
								</td>
							</tr>
							<tr>
								<th>
									Bidang Study
								</th>
								<td id="nama-bidang-studi">
									
								</td>
							</tr>
							<tr>
								<th>
									Jenjang
								</th>
								<td id="jenjang">
									SMA 1-3
								</td>
							</tr>
						</table>
						
						  <div class="card-body">
							<div class="form-group row" style="padding-top: 10px;">
									<a href="https://www.facebook.com">
										<div class="w-100 py-2 mb-2 btn btn-outline-danger rounded-4" id="email" type="submit">Email : edward@gmail.com
										 </div>
									 </a>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal" style="font-style: italic;">Close</button>
				</div>
			</div>
		</div>
	</div>

	</body>
	@include("includes.footer")
	<!-- SCROLING DOWN -->
	<script>
		function showDetail(identifier,e){

			e.preventDefault();
			var id = $(identifier).data("id");
			console.log(id);
			$.ajax({
                url: "{{ url('profil/detail') }}",
                type: "GET",
                dataType : "JSON",
                data:{
					"_token": "{{ csrf_token() }}",
                    "bidang_studi" : id
                },
                success: function(data) {     
					$("#img-profil-detail").attr("src","{{ url('storage/app/photo_profile')}}" + "/" + data.data.photos);
					$("#nama-lengkap").text(data.data.name);
					$("#riwayat-pendidikan").text(data.data.riwayat_pendidikan);
					$("#jenjang").text(data.data.jenjang);
					$("#email").text(data.data.email);
					$("#nama-bidang-studi").text(data.data.nama_bidang_studi);
					$("#viewdetail").modal("show");
                },
                error:function(xhr){
                    console.log(xhr);
                },
                cache: false
            });
		}
		
	</script>
	@include("includes.script")
</html>