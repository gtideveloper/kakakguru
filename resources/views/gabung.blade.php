<!DOCTYPE html>
<html>
	<head>
		<title>KakakGUru | Home</title>
		
		@include("includes.head")
	</head>

	<body>
		<nav class="navbar navbar-expand-lg navbar-light mb-1" style="background-color: #42eeee;">
            <div class="container-fluid menu">
               <img src="{{ url('public/assets/images/akangguru.png')}}" style="width: 60px; height: 50px;">
               <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                 <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                         <li class="nav-item">
                             <a class="nav-link collorgrey" href="#">Tentang KakakGuru</a>
                       </li>
                     </ul>               
               </div>
             </div>
       </nav>
		<nav>
			<div class="row">
				<div id="left-content" class="col s5">
	          		<div class="image">
			        	<img src="{{ url('public/assets/images/teachus.png')}}">
			      	</div>
	        	</div>
				<div id="login" class="col s7">
					<div class="card p-4 shadow-lg border-0 my-4">
						<div class="form-group row">
							<center><p class="bold">Kamu Sebagai Guru atau Murid? <br/> Jika sudah memilih tidak dapat di ganti!</p></center>
							<hr>
							<div class="col-md-6">
								<center><img src="{{ url('public/assets/images/slider4.png')}}" style="height: 100px; width: 120px;"></center>
								<p class="my-4 text-center small"><a href="{{ url('login?s=guru') }}"><button class="btn btn-primary rounded-pill">Saya Guru</button></a></p>
							</div>
							<div class="col-md-6">
								<center><img src="{{ url('public/assets/images/slider1.png')}}" style="height: 100px; width: 120px;"></center>
								<p class="my-4 text-center small"><a href="{{ url('login?s=murid') }}"><button class="btn btn-danger rounded-pill">Saya Murid</button></a></p>
							</div>
							<div class="col-md-12 px-3">
								<center><a href="{{ url('/') }}"><button class="btn btn-secondary rounded-pill" style="width:70%">Kembali</button></a></p>
							</div>
							
						</div>
					</div>
	          </div>
				
          	</div>
		</nav>
	</body>
	<script src="bootstrap/js/bootstrap.js"></script>
</html>