@component('mail::message')
<img src="{{ url('public/assets/images/teachus.png') }}" />

Hai {{ $data['nama_depan']." ".$data['nama_belakang'] }}, Klik "Verifikasi Email Sekarang" untuk mengaktifkan akunmu.

@component('mail::button', ['url' => url("verified_process")."/".$data['mail_token']])
    Link Aktivasi
@endcomponent
Kode Verifikasi ini akan berakhir dalam waktu 24 Jam. Bila kode ini tidak berfungsi atau sudah berakhir masa berlakunya, Silahkan untuk melakukan verifikasi ulang..
Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent