<!DOCTYPE html>
<html>
	<head>
		@include('includes.head')
	</head>

	<body>
		<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #42eeee;">
			<div class="container-fluid menu">
			   <img src="{{ url('public/assets/images/akangguru.png')}}" style="width: 60px; height: 50px;">
			 
			   <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				 <span class="navbar-toggler-icon"></span>
			   </button>
			   <div class="collapse navbar-collapse" id="navbarSupportedContent">
				 <ul class="navbar-nav me-auto mb-2 mb-lg-0">
				   <li class="nav-item">
					 <a class="nav-link collorgrey" href="#">Tentang KakakGuru</a>
				   </li>
				 </ul>

			   </div>
			 </div>
	   </nav>
		<nav>
			<div class="row">
				<div id="left-content" class="col s5">
	          		<div class="image">
			        	<img src="{{ url('public/assets/images/teachus.png') }}">
			      	</div>
	        	</div>
				<div id="right-content" class="col s7">
				
	           		<div class="card p-4 shadow-lg border-0 my-4">
				
					@if($request->s == "murid")
						<center><img src="{{ url('public/assets/images/murid1.png') }}" style="height: 60px; width: 80px;"></center>
						<h4 class="py-3 text-center" style="font-style: italic;">Daftar Sebagai Murid</h4>
					@elseif($request->s == "guru")
						<center><img src="{{ url('public/assets/images/guru1.png') }}" style="height: 60px; width: 80px;"></center>
						<h4 class="py-3 text-center" style="font-style: italic;">Daftar Sebagai Guru</h4>
					@endif
					<form method="POST" action="{{ route('register') }}">
						
                        @csrf
					<div class="form-group row">
						<input type="hidden" name="role_id" value="{{ $request->s }}"/>
						<div class="col-md-6">
							<input type="text" placeholder="Nama Depan" name="nama_depan" required class="form-control rounded-pill  @error('nama_depan') is-invalid @enderror">
							@error('nama_depan')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="col-md-6">
							<input type="text" placeholder="Nama Belakang" name="nama_belakang" required class="form-control rounded-pill @error('nama_belakang') is-invalid @enderror">
							@error('nama_belakang')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
					</div>
					<br>
					<div class="form-group">
						<input type="email" placeholder="Email" name="email" class="form-control rounded-pill @error('email') is-invalid @enderror">
						@error('email')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<br>
					<div class="form-group row">
						<div class="col-md-6">
							<input type="Password" placeholder="Password" name="password" class="form-control rounded-pill @error('password') is-invalid @enderror">
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="col-md-6">
							<input type="Password" name="password_confirmation" placeholder="Ketik Ulang Password" class="form-control rounded-pill">
						</div>
					</div>
					<br>
					<!-- <div class="form-group">
						<div class="costum-control">
							<input type="checkbox" id="CostumCheckbox1" class="costum-control-input">
							<label class="costum-control-label"for="CostumCheckbox1">Saya Setuju & Lanjutkan</label>
						</div>
					</div> -->
					<br>
					<div class="form-group">
						<button type="submit" class="btn btn-primary rounded-pill" style="width: 100%;">Daftar</button>
					</div>
					<div class="form-group back">
						<a href="{{ url('/') }}" class="btn btn-secondary rounded-pill" style="width: 100%;">kembali</a>
					</div>
				</form>
					
					<p class="my-4 text-center samll"><a href="{{ url('/login?s=').$request->s }}">Login Disini</a></p>
					<p class="my-3 text-center samll">Copyright &copy; 2021. KakakGuru</p>
				</div>
	          	</div>
	   
          	</div>
		</nav>
	</body>
</html>