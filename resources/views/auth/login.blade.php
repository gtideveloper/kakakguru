<!DOCTYPE html>
<html>
	<head>
		@include('includes.head')
	</head>

	<body>
		<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #42eeee;">
		 	<div class="container-fluid menu">
				<img src="{{ url('public/assets/images/akangguru.png')}}" style="width: 60px; height: 50px;">
			  
			    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			      <span class="navbar-toggler-icon"></span>
			    </button>
			    <div class="collapse navbar-collapse" id="navbarSupportedContent">
			      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
			        <li class="nav-item">
			          <a class="nav-link collorgrey" href="#">Tentang KakakGuru</a>
			        </li>
			      </ul>

			    </div>
	  		</div>
		</nav>
		<nav>
			<div class="row">
				<div id="left-content" class="col s5">
	          		<div class="image">
			        	<img src="{{ url('public/assets/images/teachus.png') }}">
			      	</div>
	        	</div>
				<div id="right-content" class="col s7">

	
					<div class="card p-4 shadow-lg border-0 my-4">
					
					@if($request->s == "murid")
						<center><img src="{{ url('public/assets/images/murid1.png') }}" style="height: 60px; width: 80px;"></center>
						<h4 class="py-3 text-center" style="font-style: italic;">Masuk Sebagai Murid</h4>
					@elseif($request->s == "guru")
						<center><img src="{{ url('public/assets/images/guru1.png') }}" style="height: 60px; width: 80px;"></center>
						<h4 class="py-3 text-center" style="font-style: italic;">Masuk Sebagai Guru</h4>
					@endif
					<form method="POST" action="{{ route('login') }}">
						@csrf
						<div class="form-group">
							<input type="text" placeholder="Email" name="email" class="form-control rounded-pill @error('email') is-invalid @enderror">
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<br>
						<div class="form-group">
							<input type="password" placeholder="Password" name="password" class="form-control rounded-pill @error('email') is-invalid @enderror"">
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<br>
						<!--<div class="form-group">
							<div class="costum-control">
								<input type="checkbox" id="CostumCheckbox1" class="costum-control-input">
								<label class="costum-control-label"for="CostumCheckbox1">I agree & continue</label>
							</div>
						</div>-->
						<br>
						<div class="form-group">
							<button type="submit" class="btn btn-primary rounded-pill" style="width: 100%;">Masuk</button>
						</div>
						<div class="form-group back">
							<a href="{{ url('/') }}" class="btn btn-secondary rounded-pill" style="width: 100%;" >Kembali</a>
						</div>
							
					</form>
					
						<!--<div class="form-group row" style="padding-top: 10px;">
							<div class="col-md-6">
								<a href="https://www.facebook.com">
									<button class="w-100 py-2 mb-2 btn btn-outline-primary rounded-4" type="submit">Sign up with Facebook
						         	</button>
						     	</a>
							</div>
							<div class="col-md-6">
								<a href="https://www.gmail.com">
									<button class="w-100 py-2 mb-2 btn btn-outline-danger rounded-4" type="submit">Sign up with Gmail
						         	</button>
						     	</a>
							</div>
						</div>-->
					

					<p class="my-4 text-center samll"><a href="{{ url('register?s=').$request->s }}">Anda belum mempunyai Akun? Buat Disini!</a></p>
					
				</div>
	          	</div>
          	</div>
		</nav>
	</body>

</html>