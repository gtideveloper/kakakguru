@extends('layouts.app')

@section('content')
    <div class="col-lg-10">    
		<div class="card p-3">
			<div class="card-header">List Jadwal Pelajaran</div>
			<div class="card-body">
				<a href="{{ url('pelajaran/tambah_pelajaran') }}" class="btn btn-success mb-2">Tambah Mata Pelajaran</a>
				<table class="table table-bordered table-striped">
                <thead>
						<tr>
							<th>Nama Bidang Studi</th>
							<th>Kelas</th>
							<th>Jenjang</th>
							<th>Hari</th>
							<th>Jam Mulai</th>
							<th>Jam Selesai</th>
						</tr>
						
					</thead>
					<tbody>
                        @foreach ($jadwal_pelajaran as $k=>$v )
                            <tr>
                                <td>{{ $v->nama_bidang_studi }}</td>
                                <td>{{ $v->kelas }}</td>
                                <td>{{ $v->jenjang }}</td>
                                <td>{{ $hari[$v->hari] }}</td>
                                <td>{{ $v->jam_mulai }}</td>
                                <td>{{ $v->jam_selesai }}</td>
                            </tr>
                        @endforeach
					</tbody>
				</table>
			</div>
	    </div>
    </div>
@endsection
