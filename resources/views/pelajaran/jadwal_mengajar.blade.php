@extends('layouts.app')

@section('content')
<div class="col-lg-10">
	<div class="card mb-2">
		<div class="card-header">
			<div class="card-title">
				<h5>Jadwal Kelas</h5>
			</div>
			
		</div>
		<div class="card-body">
			<div class="">
				<table class="table table-striped" style="text-align: center;">
					<thead class="table-dark">
						<tr>
						<th>Tanggal Pembelian</th>
						<th>Waktu</th> 
						<th nowrap>Nomor Transaksi</th>
						<th>Jumlah Pembeli</th>
						<th>Mata Pelajaran</th>
						<th>Total Dibayar</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody>
						@foreach($class_curr as $v )
							<tr>
								<td>{{ $v->created_at }}</td>
								<td nowrap>{{ $v->hari.", ".$v->jam }}</td>
								<td nowrap>{{ $v->no_order }}</td>
								<td>1</td>
								<td>{{ $v->kelas }}</td>
								<td>{{ $v->total_harga }}</td>
								<td>
									<a href="#"><button class="btn btn-primary">Mulai Kelas</button></a>
									<!--<button type="button" class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#viewdetails">View Details</button>-->
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	{{-- <div class="card mb-2">
		<div class="card-header">
			<div class="card-title">
				<h5>Riwayat Pembelian</h5>
			</div>
			
		</div>
		<div class="card-body">
			<div class="">
				<table class="table table-striped" style="text-align: center;">
					<thead class="table-dark">
						<tr>
						<th>
							Tanggal Pembelian
						</th>
						<th>
							Waktu
						</th> 
						<th>
							Nomor Transaksi 
						</th>
						<th>
							Jumlah Pembeli 
						</th>
						<th>
							Mata Pelajaran
						</th>
						<th>
							Total Dibayar
						</th>
						<th>
							Aksi
						</th>
					</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								08/08/2021
							</td>
							<td>
								08:00 - 10:00 WIB
							</td>
							<td>
								0001
							</td>
							<td>
								1
							</td>
							<td>
								Matematika
							</td>
							<td>
								120.000,00
							</td>
							<td>
								
								<a href="#"><button class="btn btn-danger">Detail</button></a>
							</td>
						  </tr>

						  <tr>
							<td>
								08/08/2021
							</td>
							<td>
								11:00 - 13:00 WIB
							</td>
							<td>
								0002
							</td>
							<td>
								3
							</td>
							<td>
								IPA
							</td>
							<td>
								150.000,00
							</td>
							<td>
								
								<a href="#"><button class="btn btn-danger">Detail</button></a>
							</td>
						  </tr>

					</tbody>
				</table>
			</div>
		</div>
	</div> --}}
</div>

@endsection
