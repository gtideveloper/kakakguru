@extends('layouts.app')

@section('content')

    <div class="col-lg-10">
        <form method="post" >
            @csrf
        <div class="card p-3">
            <div class="card-header">Tambah Jadwal Pelajaran</div>
            <div class="card-body">
                <form name="pelajaranForm" action="/pelajaran_proses" method="post">
                    <div class="form-row">								
                        <div class="mb-3 col-md-6">
                            <label for="inputEmail4" id="jenjang" >Tingkat Pendidikan</label>
                            <select class="form-control" name="jenjang">
                                <option value="sd">SD</option>
                                <option value="smp">SMP</option>
                                <option value="sma">SMA</option>
                            </select>
                        </div>
                        
                        <div class="mb-3 col-md-6">
                            <label for="inputEmail4" >Kelas</label>
                            <select class="form-control" id="kelas" name="kelas">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        
                        <div class="mb-3 col-md-6">
                            <label for="inputEmail4">Pelajaran Yang dipilih</label>
                            <select class="form-select" id="nama_bidang_studi" name="nama_bidang_studi">
                                <option value="matematika">Matematika</option>
                                <option value="bahasa_indonesia">Bahasa Indonesia</option>
                            </select>
                       </div>

                        <div class="mb-3 col-md-4">
                            <label for="inputEmail4">Biaya Persesi</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text">Rp</span>
                                <input type="text" name="biaya" class="form-control" aria-label="Amount (to the nearest dollar)">
                                <span class="input-group-text">.00</span>
                              </div>
                           
                        </div>
                        <hr/>
                        <div class="form-group col-md-12 "> 
                            <button class="btn btn-info add-row"><i class="fas fa-plus"></i> Tambah Jadwal</button>
                        </div>
                        <div class="form-group col-md-12">

                            <table class="table" id="sample_table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Hari</th>
                                        <th>Jam Mulai</th>
                                        <th>Jam Selesai</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="row-content">
                                        <td class="no">1</td>
                                        <td>
                                            <select type="text" id="jadwal_hari" required name="jadwal_hari[]" class="form-control text-plain" value="1"/>
                                                <option value="0">Minggu</option>
                                                <option value="1">Senin</option>
                                                <option value="2">Selasa</option>
                                                <option value="3">Rabu</option>
                                                <option value="4">Kamis</option>
                                                <option value="5">Jumat</option>
                                                <option value="6">Sabtu</option>													</select>
                                        </td>
                                        <td>
                                            <input type="text" id="jadwal_waktu_mulai" name="jadwal_waktu_mulai[]" class="form-control" value="00:00" />
                                        </td>
                                        
                                        <td>
                                            <input type="text" id="jadwal_waktu_selesai" name="jadwal_waktu_selesai[]" class="form-control" value="00:00" />
                                        </td>
                                        <td><a href="#" class="delete-record" ><i class="fas fa-trash text-danger"></i></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Simpan Jadwal</button>
                  </form>
            </div>
        </div>
     </div>
</form>
     <script>
         $(document).ready(function(){
			$(".add-row").on('click', function(e) {
				e.preventDefault();    
				
				var content = $('.row-content:last'),
				size = $('#sample_table > tbody >tr').length + 1;
				var element = null;
				element = content.clone();
				element.attr('id', 'rec-'+size);
				element.find('.no').text(size);
				element.find('.delete-record').attr('data-id', size);
				element.appendTo('tbody');
				element.find('.sn').html(size);
			});

            var timepicker = new TimePicker('jadwal_waktu_mulai', {
                lang: 'en',
                theme: 'dark'
            });
            timepicker.on('change', function(evt) {
            
                var value = (evt.hour || '00') + ':' + (evt.minute || '00');
                evt.element.value = value;

            });

			$(".delete-record").on('click', function(e) {
				console.log("bhayu");
				e.preventDefault();    
				var didConfirm = confirm("Yakin Ingin Menghapus ?");
				if (didConfirm == true) {
					var id = $(this).attr('data-id');
					var targetDiv = $(this).attr('targetDiv');
					$('#rec-' + id).remove();
					
					//regnerate index number on table
					$('.row-content').each(function(index) {
						$(this).find('.no').html(index+1);
					});
					return true;
				} else {
					return false;
				}
			});
		})
     </script>
@endsection
