@extends('layouts.app')

@section('content')
<div class="col-lg-10">
    <h4>Profil Guru</h4>
    <div class="card p-3 mb-2">
        <div class="card-body">
          <div class="row">
              <div class="col-md-3">
                
                <center>
                    <img src="{{ url('public/assets/images/no-image.png') }}" class="rounded-pill mb-2" style="width:200px;height:200px"/>
                    <div>
                        <i class="material-icons prefix yellow" >star_border</i>
                        <i class="material-icons prefix yellow" >star_border</i>
                        <i class="material-icons prefix yellow" >star_border</i>
                        <i class="material-icons prefix yellow" >star_border</i>
                        <i class="material-icons prefix yellow" >star_border</i>
                    </div>
                </center>
              </div>
              <div class="col-md-7">
                  <table class="table table-bordered">
                        <tr>
                            <td>Nama</td>    
                            <td>Bayu Nugroho</td>    
                        </tr>
                        <tr>
                            <td>Email</td>    
                            <td>bhayunugroho0809@gmail.com</td>    
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>    
                            <td>Laki-laki</td>    
                        </tr>
                        <tr>
                            <td>Umur</td>    
                            <td>30 Tahun</td>    
                        </tr>
                        <tr>
                            <td>Riwayat Pendidikan</td>    
                            <td>S3, Teknik Sipil</td>    
                        </tr>
                  </table>
            </div>
          </div>
          
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="mb-4">Total Murid Belajar</h6>
                            <h2 class="text-bold">68</h2>
                        </div>
                        <div class="col-md-6 text-end pl-3 pr-3">
                            <i class="fas fa-file fa-6x text-info text-end"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h6 class="mb-4">Total Cuan</h6>
                            <h2 class="text-bold">68.000.000</h2>
                        </div>
                        <div class="col-md-4 text-end pl-3 pr-3">
                            <i class="fas fa-file fa-6x text-info text-end"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="mb-4">Total Jam Mengajar</h6>
                            <h2 class="text-bold">72 Jam</h2>
                        </div>
                        <div class="col-md-6 text-end pl-3 pr-3">
                            <i class="fas fa-file fa-6x text-info text-end"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-hover table-bordered">
                    <tr>
                        <th>Hari & Tanggal Mengajar</th>
                        <th>Nama Murid</th>
                        <th>Biaya Per Sesi</th>
                        <th>Waktu Mulai Selesai Mengajar</th>
                        <th>Waktu Tutup Selesai Mengajar</th>
                        <th>Status Pembayaran</th>
                    </tr>
                    <tr>
                        <td>Minggu, 14 Oktober 2021</td>
                        <td>Bayu Murid</td>
                        <td>15.0000.000</td>
                        <td>08:09 WIB</td>
                        <td>10:09 WIB</td>
                        <td><button class="btn btn-outline-danger">Pending</button></td>
                    </tr>
                    <tr>
                        <td>Selasa, 16 Oktober 2021</td>
                        <td>Bayu Murid</td>
                        <td>15.0000.000</td>
                        <td>08:09 WIB</td>
                        <td>10:09 WIB</td>
                        <td><button class="btn btn-outline-primary">Success</button></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
