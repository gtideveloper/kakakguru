@extends('layouts.app')

@section('content')
<div class="col-lg-10">
    <div class="card p-3 m-2">
        <div class="card-header">
            <div class="card-title">Master Guru</div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Foto Profile</th>
                        <th>Email</th>
                        <th>Nama Guru</th>
                        <th>Pendidikan Terakhir</th>
                        <th>Status</th>
                        <th>Tanggal Dibuat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- <tr>
                        <td>1</td>
                        <td><img src="{{ url('public/assets/images/no-image.png') }}" class="img rounded-pill" /></td>
                        <td>bhayunugroho0809@gmail.com</td>
                        <td>Bayu Guru</td>
                        <td>S2, Gunadarma</td>
                        <td><button class="btn btn-outline-success btn-sm rounded-pill">Aktif</button></td>
                        <td>29/10/2021</td>
                        <td class="p-3">
                            <a href="{{ url('admin/member/detail') }}" class="btn btn-outline-warning btn-sm rounded-pill"><i class="fas fa-trash-alt"></i> Detail</a>&nbsp;
                        </td>
                    </tr> --}}
                </tbody>
            </table>
        </div>
    </div>
    <div class="card p-3 m-2">
        <div class="card-header">
            <div class="card-title">Master Murid</div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Email</th>
                        <th>Nama Murid</th>
                        <th>Status</th>
                        <th>Tanggal Dibuat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- <tr>
                        <td>1</td>
                        <td>bhayunugroho0809@gmail.com</td>
                        <td>Bayu Murid</td>
                        <td>Aktif</td>
                        <td>29/10/2021</td>
                        <td>
                            <a href="{{ url('admin/member/detail_murid') }}" class="btn btn-outline-warning btn-sm rounded-pill"><i class="fas fa-trash-alt"></i> Detail</a>&nbsp;
                        </td>
                    </tr> --}}
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
