<!DOCTYPE html>
<html>
	<head>
		<title>KakakGuru</title>
		@include("includes.head")
	</head>
    <body>
        <center>
        <div class="col-md-4">
           
            <form class="border p-3 bg-white mt-5" method="POST" action="{{ route('login') }}">
                @csrf
                <fieldset>
                    <legend>Login Admin</legend>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label text-start">Email </label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password"name="password" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="mb-3 form-check">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </fieldset>
            </form>
            
        </div>
        </center>
    </body>
</html>