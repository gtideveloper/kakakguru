@extends('layouts.app')

@section('content')
<div class="col-lg-10">
    <div class="card p-3 m-2">
        <div class="card-header">
            <div class="card-title">Master Pelajaran</div>
        </div>
        <div class="card-body">
            <form action="">
                <div class="mb-3">
                    <label class="label required">Nama Pelajaran</label>
                    <input class="form-control" name="nama_pelajaran" />
                </div>
                <div class="mb-5">
                    <label class="label required">Status</label>
                    <select class="form-select">
                        <option>Aktif</option>
                        <option>Non Aktif</option>
                    </select>
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-info">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
