@extends('layouts.app')

@section('content')
<div class="col-lg-10">
    <div class="card p-3 m-2">
        <div class="card-header">
            <div class="card-title">Master Pelajaran</div>
        </div>
        <div class="card-body">
            <a href="{{ url('admin/master_pelajaran/tambah_pelajaran') }}" class="btn btn-success mb-3">Tambah</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Pelajaran</th>
                        <th>Status</th>
                        <th>Tanggal Dibuat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Matematika</td>
                        <td>Aktif</td>
                        <td>30/12/2012</td>
                        <td>
                            <button class="btn btn-info">Edit</button>&nbsp;
                            <button class="btn btn-danger">Delete</button>&nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
