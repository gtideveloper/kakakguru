<!DOCTYPE html>
<html>
	<head>
		@include("includes.head")
	</head>

	<body>
		<div class="row">
			@include('includes.sidebar')
				@yield("content")
		</div>
	</body>
	@include("includes.script")
</html>