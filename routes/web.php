<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\IntroController::class, 'index'])->name('intro');
Route::get('/gabung', [App\Http\Controllers\IntroController::class, 'gabung'])->name('gabung');
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/galery', [App\Http\Controllers\HomeController::class, 'galery'])->name('galery');
Route::get('/pelajaran', [App\Http\Controllers\PelajaranController::class, 'index'])->name('list_pelajaran');
Route::get('/jadwal_mengajar', [App\Http\Controllers\PelajaranController::class, 'jadwal_mengajar'])->name('jadwal_mengajar');
Route::get('/billing', [App\Http\Controllers\BillingController::class, 'index'])->name('billing');
Route::post('/billing', [App\Http\Controllers\BillingController::class, 'index'])->name('billing_proses');
Route::get('/pelajaran/tambah_pelajaran', [App\Http\Controllers\PelajaranController::class, 'tambah_pelajaran'])->name('pelajaran.tambah_pelajaran');
Route::post('pelajaran/tambah_pelajaran', [App\Http\Controllers\PelajaranController::class, 'tambah_pelajaran'])->name('pelajaran.tambah_pelajaran_proses');
Route::post('/update_profile', [App\Http\Controllers\HomeController::class, 'update_profile'])->name('update_profile');
Route::post('/update_pendidikan', [App\Http\Controllers\HomeController::class, 'update_pendidikan'])->name('update_pendidikan');
Route::get('/katalog_guru', [App\Http\Controllers\KatalogGuruController::class, 'index'])->name('katalog_guru');
Route::get('/cart', [App\Http\Controllers\KatalogGuruController::class, 'cart'])->name('cart');
Route::get('/checkout', [App\Http\Controllers\KatalogGuruController::class, 'checkout'])->name('checkout');
Route::post('/cart_process', [App\Http\Controllers\KatalogGuruController::class, 'cart_process'])->name('cart_process');
Route::get('/detail_user', [App\Http\Controllers\KatalogGuruController::class, 'detail_user'])->name('detail_user');
Route::get('/kelas_pesan', [App\Http\Controllers\KatalogGuruController::class, 'kelas_pesan'])->name('kelas_pesan');
Route::get('/profil', [App\Http\Controllers\ProfileController::class, 'index'])->name('profil');
Route::get('/profil/detail', [App\Http\Controllers\ProfileController::class, 'detail'])->name('detail');
Route::get('/verified_process/{mail_token}', [App\Http\Controllers\VerificationController::class, 'verified_process'])->name('verified_process');
Route::get('/admin', [App\Http\Controllers\AdminController::class, 'index'])->name('admin');
Route::get('/admin/master_pelajaran', [App\Http\Controllers\AdminController::class, 'master_pelajaran'])->name('master_pelajaran');
Route::get('/admin/master_pelajaran/tambah_pelajaran', [App\Http\Controllers\AdminController::class, 'tambah_pelajaran'])->name('tambah_pelajaran');
Route::get('/admin/master_jenjang', [App\Http\Controllers\AdminController::class, 'master_jenjang'])->name('master_jenjang');
Route::get('/admin/master_jenjang/tambah_jenjang', [App\Http\Controllers\AdminController::class, 'tambah_jenjang'])->name('tambah_jenjang');
Route::get('/admin/member', [App\Http\Controllers\AdminController::class, 'member'])->name('member');
Route::get('/admin/member/detail', [App\Http\Controllers\AdminController::class, 'detail'])->name('detail');
Route::get('/admin/member/detail_murid', [App\Http\Controllers\AdminController::class, 'detail_murid'])->name('detail_murid');
